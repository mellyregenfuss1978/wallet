### Installation

---

1. Install dependencies `yarn install`

2. Start Metro server `yarn start`

3. Run on Android or iOS simulator
   - `yarn run:ios`
   - `yarn run:android`
