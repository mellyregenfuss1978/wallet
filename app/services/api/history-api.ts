import { ApisauceInstance } from "apisauce"
import { CancelTokenSource } from "axios"

import { ApiProblems, getGeneralApiProblem } from "./api-problem"
import { GetHistoryResult } from "./api.types"
export class HistoryApi {
  private api: ApisauceInstance

  constructor(api: ApisauceInstance) {
    this.api = api
  }

  async getHistory(
    address: string,
    shardIDS: number[],
    from: string,
    to: string,
    cancelToken: CancelTokenSource,
  ): Promise<GetHistoryResult> {
    const response: any = await this.api.get(
      `/addresses/${address}/history/?nearestDates=true&from=${from}&to=${to}&shard=${shardIDS.join(
        "&shard=",
      )}`.replace(/[+]/g, "%2B"),
      {},
      { cancelToken: cancelToken.token },
    )

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }
}
