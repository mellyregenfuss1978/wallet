import { ApisauceInstance } from "apisauce"

import { ApiProblems, getGeneralApiProblem } from "./api-problem"
import { GetBalancesResult } from "./api.types"

export class BalanceApi {
  private api: ApisauceInstance

  constructor(api: ApisauceInstance) {
    this.api = api
  }

  async getBalances(address: string): Promise<GetBalancesResult> {
    const response: any = await this.api.get(`/addresses/${address}/balances/`)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const data = response.data.data.shards

    try {
      return { kind: "ok", data }
    } catch (e) {
      return ApiProblems.badData
    }
  }
}
