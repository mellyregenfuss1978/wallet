import { ExchangeAgent, ExchangeAgentProposal, Fee } from "../../models"
import { UTXO } from "../../models/utxo/utxo"
import { ApiProblem } from "./api-problem"

export type HistoryItemRegularRaw = {
  hash: string
  amount: number
  txFee: number
  addresses: string[]
  direction: string
  timestamp: string
  blockNumber?: number
  shardID: number
}

export type HistoryItemCrossShardRaw = {
  hash: string
  agentFee: number
  sourceShard: {
    timestamp: string
    blockNumber?: number
    shardID: number
    amount: number
    txFee: number
    addresses: string[]
    lock: {
      hash: string
      txFee: number
      addresses: string[]
    }
  }
  destinationShard: {
    timestamp: string
    blockNumber?: number
    shardID: number
    amount: number
    txFee: number
    addresses: string[]
    lock: {
      hash: string
      txFee: number
      addresses: string[]
    }
  }
}

export type ExchangeAgentExchangeInit = {
  code: number
  uuid: string
  sourceShardMultiSigLockWindowBlocks: number
  destinationShardMultiSigLockWindowBlocks: number
  signature: string
}

export type ExchangeAgentExchangeLock = {
  code: number
}

export type ExchangeAgentExchangeProcessed = {
  code: number
  crossShardTx: string
  lockTxHash: string
  lockTxOutNum: number
  sourceShardLockTxConfirmationsCount: number
  destinationShardLockTxConfirmationsCount: number
  sourceShardLockTxConfirmationsRequired: number
  destinationShardLockTxConfirmationsRequired: number
}

/* ========================================================================== */
/*                                   Balance                                   /
/* ========================================================================== */
export type GetBalancesResult =
  | {
      kind: "ok"
      data: {
        shardID: number
        balance: number
        balanceLocked: number
      }[]
    }
  | ApiProblem

/* ========================================================================== */
/*                                Locked UTXO's                               */
/* ========================================================================== */

export type GetLockedUTXOsResult =
  | {
      kind: "ok"
      data: {
        utxos: [
          {
            hash: string
            shardId: number
            amount: number
            unlockingBlock: number
            timestamp: number
            unlockingTimestamp: number
            output: number
          },
        ]
        stats: {
          totalCount: number
          totalAmount: number
        }
        nearestDates: {
          next: string | null
          previous: string | null
        }
      }
    }
  | ApiProblem

/* ========================================================================== */
/*                                   History                                  */
/* ========================================================================== */
export type GetHistoryResult =
  | {
      kind: "ok"
      data: {
        shardsHeight: Map<string, string>
        history: {
          records: HistoryItemRegularRaw[] | HistoryItemCrossShardRaw[]
          nearestDates: {
            next: string | null
            previous: string | null
          }
        }
      }
    }
  | ApiProblem

/* ========================================================================== */
/*                                 Transaction                                */
/* ========================================================================== */
export type GetUTXOsAndFeesResult =
  | { kind: "ok"; data: { utxos: UTXO[]; fees: Fee[] } }
  | ApiProblem
export type GetRawTransactionResult = { kind: "ok"; data: string } | ApiProblem
export type PostTransactionResult = { kind: "ok"; data: string } | ApiProblem
export type GetCheckInputTransactionResult = { kind: "ok"; data: { isUsed: boolean } } | ApiProblem

/* ========================================================================== */
/*                               Exchange Agents                              */
/* ========================================================================== */
export type GetExchangeAgentsResult = { kind: "ok"; data: ExchangeAgent[] } | ApiProblem
export type GetExchangeAgentProposalResult =
  | { kind: "ok"; data: ExchangeAgentProposal }
  | ApiProblem
export type GetExchangeAgentProposalsResult =
  | { kind: "ok"; data: ExchangeAgentProposal[] }
  | ApiProblem
export type ExchangeAgentExchangeInitResult =
  | { kind: "ok"; data: ExchangeAgentExchangeInit }
  | ApiProblem
export type ExchangeAgentExchangeLockResult =
  | { kind: "ok"; data: ExchangeAgentExchangeLock }
  | ApiProblem
export type ExchangeAgentExchangeProcessedResult =
  | { kind: "ok"; data: ExchangeAgentExchangeProcessed }
  | ApiProblem
