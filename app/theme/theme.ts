import { extendTheme, theme as defaultTheme, themeTools } from "native-base"
import radii from "native-base/src/theme/base/radius"

const theme = extendTheme({
  config: {
    useSystemColorMode: true,
    dependencies: {
      "linear-gradient": require("react-native-linear-gradient").default,
    },
  },
  dependencies: {
    "linear-gradient": require("react-native-linear-gradient").default,
  },
  colors: {
    white: "#ffffff",
    whiteAlpha: {
      50: "rgba(255, 255, 255, 0.05)",
      100: "rgba(255, 255, 255, 0.1)",
      200: "rgba(255, 255, 255, 0.2)",
      300: "rgba(255, 255, 255, 0.3)",
      400: "rgba(255, 255, 255, 0.4)",
      500: "rgba(255, 255, 255, 0.5)",
      700: "rgba(255, 255, 255, 0.7)",
      900: "rgba(255, 255, 255, 0.9)",
    },
    offWhite: "#fcfcfc",
    black: "#000000",
    dark: {
      500: "#141518",
    },
    darkGray: {
      500: "#202028",
    },
    darkGrayAlpha: {
      500: "rgba(32,32,40, 0.5)",
    },
    red: {
      100: "#FEE5D9",
      200: "#FDC6B3",
      300: "#F99F8C",
      400: "#F37A6F",
      500: "#EB4141",
      600: "#CA2F3C",
      700: "#A92038",
      800: "#881432",
      900: "#700C2F",
    },
    primary: {
      100: "#B9BCD5",
      200: "#8A90B9",
      300: "#5B649C",
      400: "#384287",
      500: "#152172",
      600: "#121D6A",
      700: "#0F185F",
      800: "#0C1455",
      900: "#060B42",
    },
    primaryAlpha: {
      50: "rgba(21, 33, 114, 0.05)",
      100: "rgba(21, 33, 114, 0.1)",
      200: "rgba(21, 33, 114, 0.2)",
      300: "rgba(21, 33, 114, 0.3)",
      400: "rgba(21, 33, 114, 0.4)",
      500: "rgba(21, 33, 114, 0.5)",
      700: "rgba(21, 33, 114, 0.7)",
    },
    coins: {
      btc: {
        100: "#ffd97d",
        300: "#ffc94b",
        500: "#f2a900",
      },
      jax: {
        100: "#B9CBED",
        300: "#839AC3",
        500: "#4C79CB",
      },
      jxn: {
        100: "#FFCE79",
        300: "#E6A430",
        500: "#E7A634",
      },
    },
  },
  radii: {
    ...radii,
    md: 8,
    lg: 12,
    xl: 16,
    "2xl": 20,
    "3xl": 24,
  },
  shadows: {
    "1": {
      shadowColor: "#000000",
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0.05,
      shadowRadius: 15,
      elevation: 2,
    },
    "2": {
      shadowColor: "#000000",
      shadowOffset: { width: 0, height: 0 },
      shadowOpacity: 0.1,
      shadowRadius: 15,
      elevation: 2,
    },
  },
  fonts: {
    heading: "SpaceGrotesk",
    body: "SpaceGrotesk",
    mono: "SpaceGrotesk",
  },
  fontConfig: {
    SpaceGrotesk: {
      100: {
        normal: "SpaceGrotesk-Light",
      },
      200: {
        normal: "SpaceGrotesk-Light",
      },
      300: {
        normal: "SpaceGrotesk-Light",
      },
      400: {
        normal: "SpaceGrotesk-Regular",
      },
      500: {
        normal: "SpaceGrotesk-Medium",
      },
      600: {
        normal: "SpaceGrotesk-Medium",
      },
      700: {
        normal: "SpaceGrotesk-SemiBold",
      },
      800: {
        normal: "SpaceGrotesk-SemiBold",
      },
      900: {
        normal: "SpaceGrotesk-Bold",
      },
    },
  },
  components: {
    Container: {
      baseStyle: (props: any) => {
        return {
          backgroundColor: themeTools.mode("offWhite", "dark.500")(props),
        }
      },
    },
    Divider: {
      baseStyle: (props: any) => {
        return {
          backgroundColor: themeTools.mode("primaryAlpha.50", "whiteAlpha.50")(props),
        }
      },
    },
    Icon: {
      baseStyle: (props) => {
        return {
          color: themeTools.mode("primary.500", "white")(props),
        }
      },
    },
    Button: {
      sizes: {
        lg: {
          minHeight: "64px",
          borderRadius: "lg",
          _text: {
            fontSize: "16px",
            fontWeight: 500,
          },
        },
        md: {
          minHeight: "48px",
          borderRadius: "md",
          px: 3,
          _text: {
            fontSize: "14px",
            fontWeight: 500,
          },
        },
        sm: {
          px: 3,
        },
      },
      variants: {
        ghost: (props) => {
          return {
            backgroundColor: themeTools.mode("transparent", "transparent")(props),
            _text: {
              color: themeTools.mode("primary.500", "white")(props),
            },
            _pressed: {
              opacity: 0.6,
            },
          }
        },
        subtle: (props) => {
          return {
            borderWidth: 1,
            borderColor: themeTools.mode("primaryAlpha.50", "whiteAlpha.50")(props),
            _android: {
              borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.100")(props),
            },
            backgroundColor: themeTools.mode("offWhite", "darkGray.500")(props),
            _text: {
              color: themeTools.mode("primary.500", "white")(props),
            },
            _pressed: {
              opacity: 0.6,
              _ios: {
                borderColor: themeTools.mode("primaryAlpha.50", "whiteAlpha.50")(props),
              },
              _android: {
                borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.100")(props),
              },
              _text: {
                color: themeTools.mode("primary.500", "white")(props),
              },
            },
          }
        },
        outline: (props) => {
          return {
            backgroundColor: themeTools.mode("offWhite", "whiteAlpha.50")(props),
            borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.50")(props),
            _android: {
              borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.100")(props),
            },
            _text: {
              color: themeTools.mode("primary.500", "white")(props),
            },
            _pressed: {
              opacity: 0.6,
              _ios: {
                borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.50")(props),
              },
              _android: {
                borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.100")(props),
              },
            },
          }
        },
        solid: (props) => {
          return {
            backgroundColor: themeTools.mode("primary.500", "primary.500")(props),
            _text: {
              color: "white",
            },
            _pressed: {
              opacity: 0.8,
            },
            _disabled: {
              _text: {
                color: themeTools.mode("primaryAlpha.300", "whiteAlpha.500")(props),
              },
              opacity: 1,
              backgroundColor: themeTools.mode("primaryAlpha.50", "darkGray.500")(props),
              borderWidth: 1,
              borderColor: themeTools.mode("primaryAlpha.50", "whiteAlpha.100")(props),
              _android: {
                borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.100")(props),
              },
            },
          }
        },
      },
    },
    Text: {
      baseStyle: (props) => {
        return {
          ...defaultTheme.components.Text.baseStyle,
          color: themeTools.mode("primary.500", "white")(props),
          fontWeight: 500,
          fontSize: "md",
        }
      },
    },
    Input: {
      baseStyle: (props) => {
        return {
          ...defaultTheme.components.Input.baseStyle,
          color: themeTools.mode("primary.500", "white")(props),
          placeholderTextColor: themeTools.mode("primaryAlpha.500", "whiteAlpha.500")(props),
        }
      },
    },
    TextArea: {
      baseStyle: (props) => {
        return {
          ...defaultTheme.components.TextArea.baseStyle,
          color: themeTools.mode("primary.500", "white")(props),
          placeholderTextColor: themeTools.mode("primaryAlpha.500", "whiteAlpha.500")(props),
        }
      },
    },
    ActionsheetContent: {
      baseStyle: (props: any) => {
        return {
          ...defaultTheme.components.ActionsheetContent.baseStyle,
          backgroundColor: themeTools.mode("offWhite", "darkGray.500")(props),
        }
      },
    },
    ActionsheetItem: {
      baseStyle: (props: any) => {
        return {
          ...defaultTheme.components.ActionsheetItem.baseStyle,
          _text: {
            color: themeTools.mode("primary.500", "white")(props),
          },
        }
      },
    },
    Select: {
      baseStyle: {
        fontSize: "16px",
      },
    },
    PopoverContent: {
      baseStyle: (props: any) => {
        return {
          ...defaultTheme.components.PopoverContent.baseStyle,
          borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.50")(props),
          backgroundColor: themeTools.mode("offWhite", "dark.500")(props),
        }
      },
    },
    PopoverArrow: {
      baseStyle: (props: any) => {
        return {
          ...defaultTheme.components.PopoverArrow.baseStyle,
          borderColor: themeTools.mode("primaryAlpha.100", "whiteAlpha.50")(props),
        }
      },
    },
    PopoverBody: {
      baseStyle: (props: any) => {
        return {
          ...defaultTheme.components.PopoverBody.baseStyle,
          py: "2",
          px: "3",
        }
      },
    },
  },
})

export { theme }
