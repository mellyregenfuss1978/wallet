interface IEnv {
  MAINNET_API_URL: string
  TESTNET_API_URL: string
}

const Env: IEnv = {
  MAINNET_API_URL: "https://api.peacewallet.io/api/v1",
  TESTNET_API_URL: "https://testnet.api.peacewallet.io/api/v1",
}

export default Env
