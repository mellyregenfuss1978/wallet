import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { ExchangeAgentAddressModel } from "../exchange-agent-address/exchange-agent-address"
import {
  ExchangeAgentProposal,
  ExchangeAgentProposalModel
} from "../exchange-agent-proposal/exchange-agent-proposal"

export const ExchangeAgentModel = types
  .model("ExchangeAgent")
  .props({
    id: types.identifier,
    publicKey: types.string,
    addresses: types.array(ExchangeAgentAddressModel),
    proposal: types.maybeNull(ExchangeAgentProposalModel),
  })
  .actions((self) => {
    const updateProposal = (proposal: ExchangeAgentProposal) => {
      self.proposal = proposal
    }

    return {
      updateProposal,
    }
  })

type ExchangeAgentType = Instance<typeof ExchangeAgentModel>
export interface ExchangeAgent extends ExchangeAgentType {}
type ExchangeAgentSnapshotType = SnapshotOut<typeof ExchangeAgentModel>
export interface ExchangeAgentSnapshot extends ExchangeAgentSnapshotType {}
export const createExchangeAgentDefaultModel = () => types.optional(ExchangeAgentModel, {})
