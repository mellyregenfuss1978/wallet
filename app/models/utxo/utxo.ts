import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const UTXOModel = types.model("UTXO").props({
  hash: types.string,
  output: types.number,
  amount: types.number,
  pkScript: types.string,
  raw: types.maybeNull(types.string),
})

type UTXOType = Instance<typeof UTXOModel>
export interface UTXO extends UTXOType {}
type UTXOSnapshotType = SnapshotOut<typeof UTXOModel>
export interface UTXOSnapshot extends UTXOSnapshotType {}
export const createBalanceByShardDefaultModel = () => types.optional(UTXOModel, {})
