import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const CoinModel = types.model("Coin").props({
  name: types.identifier,
  title: types.string,
  shardID: types.maybeNull(types.number),
})

type CoinType = Instance<typeof CoinModel>
export interface Coin extends CoinType {}
type CoinSnapshotType = SnapshotOut<typeof CoinModel>
export interface CoinSnapshot extends CoinSnapshotType {}
export const createCoinDefaultModel = () => types.optional(CoinModel, {})
