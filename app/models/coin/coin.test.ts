import { CoinModel } from "./coin"

test("can be created", () => {
  const instance = CoinModel.create({
    name: "btc",
    title: "Bitcoin",
  })

  expect(instance).toBeTruthy()
})
