import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import moment from "moment"

import { ExchangeAgent, ExchangeAgentModel } from "../exchange-agent/exchange-agent"
import { withEnvironment } from "../extensions/with-environment"
import { StoreError, withError } from "../extensions/with-error"
import { withStatus } from "../extensions/with-status"

export const ExchangeAgentStoreModel = types
  .model("ExchangeAgentStore")
  .props({
    exchangeAgents: types.array(ExchangeAgentModel),
    selectedExchangeAgent: types.maybeNull(types.safeReference(ExchangeAgentModel)),
  })
  .extend(withError)
  .extend(withStatus)
  .extend(withEnvironment)
  .views((self) => {
    return {
      get exchangeAgentsWithoutProposals(): ExchangeAgent[] {
        return self.exchangeAgents.filter((exchangeAgent) => !exchangeAgent.proposal)
      },

      exchangeAgentsSortedByLowestFee(amount: number): ExchangeAgent[] {
        return self.exchangeAgents
          .slice()
          .filter(
            (exchangeAgent) =>
              exchangeAgent.proposal && exchangeAgent.proposal.reservedAmount >= amount,
          )
          .sort(
            (a, b) =>
              (a.proposal ? a.proposal.calculateFee(amount) : 0) -
              (b.proposal ? b.proposal.calculateFee(amount) : 0),
          )
      },
    }
  })
  .actions((self) => {
    const selectExchangeAgent = (exchangeAgent: ExchangeAgent) => {
      self.selectedExchangeAgent = exchangeAgent
    }

    const handleError = (error: StoreError) => {
      self.setStatus("error")
      self.setError(error)
    }

    const saveExchangeAgents = (exchangeAgents: ExchangeAgent[]) => {
      self.exchangeAgents.replace(exchangeAgents)
    }

    const getExchangeAgents = flow(function* getExchangeAgents(
      sourceShardId: number,
      destinationShardId: number,
    ) {
      self.setError(null)
      self.setStatus("pending")

      self.exchangeAgents.clear()

      yield fillExchangeAgentsReserves(sourceShardId, destinationShardId, self.exchangeAgents)

      const result = yield self.environment.api.exchangeAgent.getExchangeAgents(
        sourceShardId,
        destinationShardId,
      )

      if (result.kind === "ok") {
        const exchangeAgentsWithHTTPS = clearExchangeAgentsWithoutHTTPS(result.data)
        const exchangeAgentsWithProposals = yield fillExchangeAgentsProposals(
          sourceShardId,
          destinationShardId,
          exchangeAgentsWithHTTPS,
        )
        const exchangeAgentsWithReserves = yield fillExchangeAgentsReserves(
          sourceShardId,
          destinationShardId,
          exchangeAgentsWithProposals,
        )
        saveExchangeAgents(exchangeAgentsWithReserves)
        self.setStatus("done")
      } else {
        handleError(result)
      }
    })

    const clearExchangeAgentsWithoutHTTPS = (exchangeAgents: ExchangeAgent[]): ExchangeAgent[] => {
      const _exchangeAgents = []

      exchangeAgents.forEach((exchangeAgent) => {
        const addresses = exchangeAgent.addresses.filter(
          (address) => address.url && address.url.length > 0,
        )

        if (addresses.length > 0) {
          const formattedExchangeAgent = {
            ...exchangeAgent,
            addresses: addresses,
          }
          _exchangeAgents.push(formattedExchangeAgent)
        }
      })

      return _exchangeAgents
    }

    const fillExchangeAgentsProposals = async (
      sourceShardId: number,
      destinationShardId: number,
      exchangeAgents: ExchangeAgent[],
    ) => {
      self.setError(null)

      const exchangeAgentsWithoutProposals = exchangeAgents.filter(
        (exchangeAgent) => !exchangeAgent.proposal,
      )

      const randomIndex = Math.floor(Math.random() * exchangeAgentsWithoutProposals.length)
      const exchangeAgent = exchangeAgentsWithoutProposals[randomIndex]

      if (exchangeAgentsWithoutProposals.length <= 0) {
        return exchangeAgents
      }

      const result = await self.environment.api.exchangeAgent.getExchangeAgentProposals(
        exchangeAgent.addresses[0].url,
        sourceShardId,
        destinationShardId,
      )

      if (result.kind === "ok") {
        const proposals = result.data

        proposals.forEach((proposal) => {
          const exchangeAgent = exchangeAgents.find(
            (exchangeAgent) => exchangeAgent.id === proposal.exchangeAgentId,
          )

          if (!exchangeAgent) return

          if (moment(proposal.expires).isBefore(moment())) {
            exchangeAgents.splice(exchangeAgents.indexOf(exchangeAgent), 1)
          } else {
            exchangeAgent.proposal = proposal
          }
        })

        if (!proposals.find((proposal) => proposal.exchangeAgentId === exchangeAgent.id)) {
          exchangeAgents.splice(exchangeAgents.indexOf(exchangeAgent), 1)
        }

        if (exchangeAgentsWithoutProposals.length > 0) {
          exchangeAgents = await fillExchangeAgentsProposals(
            sourceShardId,
            destinationShardId,
            exchangeAgents,
          )
        }
      } else {
        exchangeAgents.splice(exchangeAgents.indexOf(exchangeAgent), 1)
        exchangeAgents = await fillExchangeAgentsProposals(
          sourceShardId,
          destinationShardId,
          exchangeAgents,
        )
      }

      return exchangeAgents
    }

    const fillExchangeAgentsReserves = async (
      sourceShardId: number,
      destinationShardId: number,
      exchangeAgents: ExchangeAgent[],
    ) => {
      await Promise.all(
        exchangeAgents.map(async (exchangeAgent) => {
          const result = await self.environment.api.exchangeAgent.getExchangeAgentProposal(
            exchangeAgent.addresses[0].url,
            sourceShardId,
            destinationShardId,
          )

          if (result.kind === "ok") {
            if (exchangeAgent.proposal?.exchangeAgentId) {
              exchangeAgent.proposal = { ...exchangeAgent.proposal, ...result.data }
            }
          } else {
            exchangeAgents.splice(exchangeAgents.indexOf(exchangeAgent), 1)
          }
        }),
      )

      return exchangeAgents
    }

    return {
      selectExchangeAgent,
      getExchangeAgents,
    }
  })

type ExchangeAgentStoreType = Instance<typeof ExchangeAgentStoreModel>
export interface ExchangeAgentStore extends ExchangeAgentStoreType {}
type ExchangeAgentStoreSnapshotType = SnapshotOut<typeof ExchangeAgentStoreModel>
export interface ExchangeAgentStoreSnapshot extends ExchangeAgentStoreSnapshotType {}
export const createExchangeAgentStoreDefaultModel = () =>
  types.optional(ExchangeAgentStoreModel, {})
