import { CancelToken } from "apisauce"
import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import moment from "moment"

import {
  GetHistoryResult,
  HistoryItemCrossShardRaw,
  HistoryItemRegularRaw,
} from "../../services/api/api.types"
import { StoreError, withEnvironment, withError, withStatus } from "../extensions"
import { HistoryItem, HistoryItemModel } from "../history-item/history-item"
import { HistoryModel } from "../history/history"

export const HistoryStoreModel = types
  .model("HistoryStore")
  .props({
    history: types.map(HistoryModel),
    localHistory: types.map(HistoryModel),
    nearestPreviousDate: types.maybeNull(types.string),
    loadedAllPreviousItems: types.optional(types.boolean, false),
    historyLength: types.optional(types.number, 0),
  })
  .extend(withError)
  .extend(withStatus)
  .extend(withEnvironment)
  .volatile((self) => ({
    cancelToken: CancelToken.source(),
  }))
  .views((self) => ({
    historyByDate: (shardIDs: number[]) => {
      const sections = []
      shardIDs.forEach((shardID) => {
        const items = Array.from(self.history.get(shardID.toString())?.items.values() ?? [])

        items.forEach((item) => {
          const date = moment(item.timestamp).format("YYYY-MM-DD")

          const section = sections.find((_section) => _section.title === date)
          if (section) {
            section.items.push(item)
            section.items.sort((a, b) => (moment(a.timestamp) < moment(b.timestamp) ? 1 : -1))
          } else {
            sections.push({ title: date, items: [item] })
          }

          sections.sort((a, b) => (moment(a.title) < moment(b.title) ? 1 : -1))
        })
      })

      return sections
    },
  }))
  .actions((self) => {
    const handleError = (error: StoreError) => {
      self.setStatus("error")
      self.setError(error)
    }

    const updateHistoryLength = (length: number) => {
      self.historyLength = length
    }

    const addOrUpdate = (shardID: number, item: HistoryItem) => {
      if (!self.history.has(shardID.toString())) {
        self.history.set(shardID.toString(), { shardID: shardID, items: {} })
      }

      self.history.get(shardID.toString()).items.set(item.transactionHash, {
        ...self.history.get(shardID.toString()).items.get(item.transactionHash),
        ...item,
        crossShardTemp: item.crossShardTemp ? { ...item.crossShardTemp } : null,
      })
    }

    const addOrUpdateLocal = (shardID: number, item: HistoryItem) => {
      if (!self.localHistory.has(shardID.toString())) {
        self.localHistory.set(shardID.toString(), { shardID: shardID, items: {} })
      }

      self.localHistory.get(shardID.toString()).items.set(item.transactionHash, {
        ...self.localHistory.get(shardID.toString()).items.get(item.transactionHash),
        ...item,
        crossShardTemp: item.crossShardTemp ? { ...item.crossShardTemp } : null,
      })

      addOrUpdate(shardID, item)
    }

    const update = (shardID: number, txHash: string, params: Partial<HistoryItem>) => {
      self.history.get(shardID.toString()).items.set(txHash, {
        ...self.localHistory.get(shardID.toString()).items.get(txHash),
        ...params,
      })
    }

    const updateLocal = (shardID: number, txHash: string, params: Partial<HistoryItem>) => {
      self.localHistory.get(shardID.toString()).items.set(txHash, {
        ...self.localHistory.get(shardID.toString()).items.get(txHash),
        ...params,
      })

      update(shardID, txHash, params)
    }

    const remove = (shardID: number, transactionHash: string) => {
      self.history.get(shardID.toString()).items.delete(transactionHash)
    }

    const removeLocal = (shardID: number, transactionHash: string) => {
      self.localHistory.get(shardID.toString()).items.delete(transactionHash)

      remove(shardID, transactionHash)
    }

    const getHistory = flow(function* getHistory(
      address: string,
      shardIDS: number[],
      refresh = true,
      silentRefresh = false,
    ) {
      self.setStatus("pending")
      self.setError(null)

      let from = null
      let to = null

      if (refresh || silentRefresh) {
        from = moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).format()
        to = moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).add(1, "day").format()
        self.loadedAllPreviousItems = false
      } else {
        self.historyByDate(shardIDS).forEach((item) => {
          if (!self.nearestPreviousDate) {
            self.nearestPreviousDate = item.title
          } else if (moment(item.title).isBefore(moment(self.nearestPreviousDate))) {
            self.nearestPreviousDate = item.title
          }
        })

        from = moment(self.nearestPreviousDate)
          .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          .format()
        to = moment(self.nearestPreviousDate)
          .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          .add(1, "day")
          .format()
      }

      if (!moment(from).isValid() && !moment(to).isValid()) {
        return null
      }

      const result: GetHistoryResult = yield self.environment.api.history.getHistory(
        address,
        shardIDS,
        from,
        to,
        refresh,
      )

      if (result.kind === "ok") {
        if (!silentRefresh) {
          const nearestPreviousDate = result.data.history.nearestDates.previous

          if (nearestPreviousDate) {
            self.nearestPreviousDate = nearestPreviousDate
          } else {
            self.loadedAllPreviousItems = true

            self.localHistory.forEach((history) => {
              history.items.forEach((historyItem) => {
                addOrUpdate(history.shardID, historyItem)
              })
            })
          }
        }

        result.data.history.records.forEach((record) => {
          if (record.sourceShard && record.destinationShard) {
            const item: HistoryItemCrossShardRaw = record

            const direction = item.destinationShard.addresses.includes(address) ? "in" : "out"
            const _address =
              item[direction === "out" ? "destinationShard" : "sourceShard"].addresses[0]
            const shardID = item[direction === "out" ? "sourceShard" : "destinationShard"].shardID
            const timestamp =
              item[direction === "out" ? "sourceShard" : "destinationShard"].timestamp
            const blockNumber =
              item[direction === "out" ? "sourceShard" : "destinationShard"].blockNumber
            const transactionFee =
              item[direction === "out" ? "sourceShard" : "destinationShard"].lock.txFee
            const lockTransactionHash =
              item[direction === "out" ? "sourceShard" : "destinationShard"].lock.hash

            let confirmations = blockNumber
              ? result.data.shardsHeight[shardID] + 1 - blockNumber
              : 0
            confirmations = confirmations > 0 ? confirmations : 0

            if (self.localHistory.get(item.sourceShard.shardID.toString())?.items.get(item.hash)) {
              removeLocal(item.sourceShard.shardID, item.hash)
            }

            self.localHistory.forEach((history) => {
              history.items.forEach((historyItem) => {
                if (moment(historyItem.timestamp).isSameOrAfter(timestamp, "day")) {
                  addOrUpdate(history.shardID, historyItem)
                }
              })
            })

            addOrUpdate(
              item.sourceShard.shardID,
              HistoryItemModel.create({
                transactionHash: item.hash,
                sourceAmount: item.sourceShard.amount,
                destinationAmount: item.destinationShard.amount,
                exchangeAgentFee: item.agentFee,
                transactionFee: transactionFee,
                address: _address,
                sourceShardID: item.sourceShard.shardID,
                destinationShardID: item.destinationShard.shardID,
                direction: direction,
                timestamp: timestamp,
                blockNumber: blockNumber,
                confirmations: confirmations,
                lockTransactionHash: lockTransactionHash,
              }),
            )
          } else if (!record.sourceShard && !record.destinationShard) {
            const item: HistoryItemRegularRaw = record

            let confirmations = item.blockNumber
              ? result.data.shardsHeight[item.shardID] + 1 - item.blockNumber
              : 0
            confirmations = confirmations < 0 ? 0 : confirmations

            if (self.localHistory.get(item.shardID.toString())?.items.get(item.hash)) {
              removeLocal(item.shardID, item.hash)
            }

            self.localHistory.forEach((history) => {
              history.items.forEach((historyItem) => {
                if (moment(historyItem.timestamp).isSameOrAfter(moment(item.timestamp), "day")) {
                  addOrUpdate(history.shardID, historyItem)
                }
              })
            })

            addOrUpdate(
              item.shardID,
              HistoryItemModel.create({
                transactionHash: item.hash,
                sourceAmount: item.amount,
                destinationAmount: item.amount,
                transactionFee: item.txFee,
                address: item.addresses[0],
                sourceShardID: item.shardID,
                destinationShardID: item.shardID,
                direction: item.direction,
                timestamp: item.timestamp,
                blockNumber: item.blockNumber,
                confirmations: confirmations,
              }),
            )
          }
        })

        self.setStatus("done")
      } else {
        handleError(result)
      }

      return result
    })

    const reset = () => {
      self.history.clear()
      self.localHistory.clear()

      self.nearestPreviousDate = null
      self.loadedAllPreviousItems = false
    }

    const clear = (shardIDS: number[]) => {
      shardIDS.forEach((shardID) => self.history.get(shardID.toString())?.items.clear())

      self.nearestPreviousDate = null
      self.loadedAllPreviousItems = false
    }

    return {
      addOrUpdate,
      addOrUpdateLocal,
      update,
      updateLocal,
      remove,
      removeLocal,
      getHistory,
      reset,
      clear,
      updateHistoryLength,
    }
  })

type HistoryStoreType = Instance<typeof HistoryStoreModel>
export interface HistoryStore extends HistoryStoreType {}
type HistoryStoreSnapshotType = SnapshotOut<typeof HistoryStoreModel>
export interface HistoryStoreSnapshot extends HistoryStoreSnapshotType {}
export const createHistoryStoreDefaultModel = () => types.optional(HistoryStoreModel, {})
