import { ExchangeAgentProposalModel } from "./exchange-agent-proposal"

test("can be created", () => {
  const instance = ExchangeAgentProposalModel.create({
    exchangeAgentId: "15011669113305636526",
    reservedAmount: 200000000000,
    feeFixedAmount: 10,
    feePercents: 0,
  })

  expect(instance).toBeTruthy()
})
