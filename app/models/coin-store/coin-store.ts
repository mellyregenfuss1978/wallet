import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { Coin, CoinModel } from "../coin/coin"
import { withEnvironment } from "../extensions/with-environment"

export const CoinStoreModel = types
  .model("CoinStore")
  .props({
    coins: types.array(CoinModel),
    selectedCoin: types.reference(CoinModel),
  })
  .extend(withEnvironment)
  .views((self) => ({}))
  .actions((self) => {
    const selectCoin = (coin: Coin) => {
      self.selectedCoin = coin
    }

    return {
      selectCoin,
    }
  })

type CoinStoreType = Instance<typeof CoinStoreModel>
export interface CoinStore extends CoinStoreType {}
type CoinStoreSnapshotType = SnapshotOut<typeof CoinStoreModel>
export interface CoinStoreSnapshot extends CoinStoreSnapshotType {}
export const createCoinStoreDefaultModel = () =>
  types.optional(CoinStoreModel, {
    coins: [
      {
        name: "btc",
        title: "BTC",
        shardID: Math.pow(2, 32) - 1,
      },
      {
        name: "jax",
        title: "JAX",
        shardID: null,
      },
      {
        name: "jxn",
        title: "JXN",
        shardID: 0,
      },
    ],
    selectedCoin: "jax",
  })
