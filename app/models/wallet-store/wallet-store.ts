import * as bip39 from "bip39"
import * as bitcoin from "bitcoinjs-lib"
import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"

import { withEnvironment } from "../extensions/with-environment"
import { withRootStore } from "../extensions/with-root-store"
import { withStatus } from "../extensions/with-status"

const MAINNET_PATH = "m/44'/0'/0'/0/0"
const TESTNET_PATH = "m/44'/1'/0'/0/0"

export const WalletStoreModel = types
  .model("WalletStore")
  .props({
    mnemonic: types.maybeNull(types.string),
    address: types.maybeNull(types.string),
    privateKey: types.maybeNull(types.string),
    wif: types.maybeNull(types.string),
  })
  .extend(withStatus)
  .extend(withEnvironment)
  .extend(withRootStore)
  .views((self) => {
    return {
      get node(): bitcoin.BIP32Interface {
        const path = self.rootStore.networkName === "testnet" ? TESTNET_PATH : MAINNET_PATH

        let seed = null
        let node = null

        if (self.mnemonic) {
          seed = bip39.mnemonicToSeedSync(self.mnemonic)
          node = bitcoin.bip32.fromSeed(seed).derivePath(path)
        } else if (self.privateKey) {
          node = bitcoin.ECPair.fromPrivateKey(Buffer.from(self.privateKey, "hex"), {
            compressed: false,
          })
        } else if (self.wif) {
          node = bitcoin.ECPair.fromWIF(self.wif)
        }

        return node
      },
    }
  })
  .actions((self) => {
    const generateMnemonic = () => {
      return bip39.generateMnemonic()
    }

    const saveMnemonic = flow(function* (mnemonic: string): any {
      self.setStatus("pending")

      self.mnemonic = mnemonic
      self.address = yield getAddress()

      self.setStatus("done")
    })

    const savePrivateKey = flow(function* (privateKey: string): any {
      self.setStatus("pending")

      self.privateKey = privateKey
      self.address = yield getAddress()

      self.setStatus("done")
    })

    const saveWIF = flow(function* (wif: string): any {
      self.setStatus("pending")

      self.wif = wif
      self.address = yield getAddress()

      self.setStatus("done")
    })

    const getAddress = async (): Promise<string> => {
      const path = self.rootStore.networkName === "testnet" ? TESTNET_PATH : MAINNET_PATH

      let seed = null
      let node = null

      if (self.mnemonic) {
        seed = await bip39.mnemonicToSeed(self.mnemonic)
        node = bitcoin.bip32.fromSeed(seed).derivePath(path)
      } else if (self.privateKey) {
        node = bitcoin.ECPair.fromPrivateKey(Buffer.from(self.privateKey, "hex"), {
          compressed: false,
        })
      } else if (self.wif) {
        node = bitcoin.ECPair.fromWIF(self.wif)
      }

      const address = bitcoin.payments.p2pkh({
        pubkey: node.publicKey,
        network: self.rootStore.network,
      }).address

      return address
    }

    const refreshAddress = flow(function* (): any {
      self.address = yield getAddress()
    })

    const reset = () => {
      self.address = null
      self.mnemonic = null
      self.privateKey = null
      self.wif = null
    }

    const isValidMnemonic = (mnemonic: string): boolean => {
      return bip39.validateMnemonic(mnemonic)
    }

    const isValidPrivateKey = (privateKey: string): boolean => {
      let isValid = false

      try {
        bitcoin.ECPair.fromPrivateKey(Buffer.from(privateKey, "hex"), { compressed: false })
        isValid = true
      } catch (e) {
        isValid = false
      }

      return isValid
    }

    const isValidWIF = (wif: string): boolean => {
      let isValid = false

      try {
        bitcoin.ECPair.fromWIF(wif)
        isValid = true
      } catch (e) {
        isValid = false
      }

      return isValid
    }

    const isValidAddress = (address: string): boolean => {
      try {
        bitcoin.address.toOutputScript(address, self.rootStore.network)
        return true
      } catch (e) {
        return false
      }
    }

    return {
      generateMnemonic,
      saveMnemonic,
      saveWIF,
      savePrivateKey,
      isValidMnemonic,
      isValidPrivateKey,
      isValidAddress,
      isValidWIF,
      refreshAddress,
      reset,
    }
  })

type WalletStoreType = Instance<typeof WalletStoreModel>
export interface WalletStore extends WalletStoreType {}
type WalletStoreSnapshotType = SnapshotOut<typeof WalletStoreModel>
export interface WalletStoreSnapshot extends WalletStoreSnapshotType {}
export const createWalletStoreDefaultModel = () => types.optional(WalletStoreModel, {})
