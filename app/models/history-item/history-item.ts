import { Instance, SnapshotOut, types } from "mobx-state-tree"

import {
  HistoryCrossShardTemp,
  HistoryCrossShardTempModel
} from "../history-cross-shard-temp/history-cross-shard-temp"

export const HistoryItemModel = types
  .model("HistoryItem")
  .props({
    transactionHash: types.identifier,
    lockTransactionHash: types.maybeNull(types.string),
    sourceAmount: types.number,
    destinationAmount: types.number,
    transactionFee: types.optional(types.number, 0),
    exchangeAgentFee: types.optional(types.number, 0),
    address: types.string,
    sourceShardID: types.number,
    destinationShardID: types.number,
    direction: types.enumeration(["out", "in", "loop"]),
    timestamp: types.string,
    blockNumber: types.maybeNull(types.number),
    confirmations: types.maybeNull(types.number),
    crossShardTemp: types.maybeNull(HistoryCrossShardTempModel),
    refundTransactionHex: types.maybeNull(types.string),
    refundBlocks: types.maybeNull(types.number),
  })
  .views((self) => ({
    get isCrossShard() {
      return self.sourceShardID !== self.destinationShardID
    },
    get isLock() {
      return self.address.startsWith("2")
    },
    get totalFee() {
      return self.transactionFee + self.exchangeAgentFee
    },
    get crossShardAmountByDirection() {
      return self.direction === "in"
        ? self.destinationAmount
        : self.sourceAmount + self.exchangeAgentFee
    },
  }))
  .actions((self) => {
    const updateCrossShardTemp = (params: Partial<HistoryCrossShardTemp>) => {
      self.crossShardTemp = {
        ...self.crossShardTemp,
        ...params,
      }
    }

    const clearCrossShardTemp = () => {
      self.crossShardTemp = null
    }

    return {
      updateCrossShardTemp,
      clearCrossShardTemp,
    }
  })

type HistoryItemType = Instance<typeof HistoryItemModel>
export interface HistoryItem extends HistoryItemType {}
type HistoryItemSnapshotType = SnapshotOut<typeof HistoryItemModel>
export interface HistoryItemSnapshot extends HistoryItemSnapshotType {}
export const HistoryItemDefaultModel = () => types.optional(HistoryItemModel, {})
