import { HistoryCrossShardTempModel } from "./history-cross-shard-temp"

test("can be created", () => {
  const instance = HistoryCrossShardTempModel.create({
    exchangeTxUUID: "4e716634-97a2-46f7-a641-440dbc308231",
    exchangeTxSourceShardLock: 240,
    exchangeTxDestinationShardLock: 120,
    sourceShardFundsLockingTxHash:
      "0499d2561527066a9cfe77a7a15dcced9dc69d7c949884288a3052b59b84629cd9bb74d5817a13ee0188c78f004a35bfd05827c89990b0ca287ee949e4bb5dff30",
    sourceShardMultiSigRedeemHex:
      "0499d2561527066a9cfe77a7a15dcced9dc69d7c949884288a3052b59b84629cd9bb74d5817a13ee0188c78f004a35bfd05827c89990b0ca287ee949e4bb5dff30",
    destinationShardMultiSigRedeemHex:
      "0499d2561527066a9cfe77a7a15dcced9dc69d7c949884288a3052b59b84629cd9bb74d5817a13ee0188c78f004a35bfd05827c89990b0ca287ee949e4bb5dff30",
  })

  expect(instance).toBeTruthy()
})
