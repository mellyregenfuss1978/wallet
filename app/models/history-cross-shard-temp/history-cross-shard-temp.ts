import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const HistoryCrossShardTempModel = types
  .model("HistoryCrossShardTemp")
  .props({
    // Step 0
    exchangeAgentUrl: types.maybeNull(types.string),
    exchangeAgentPublicKey: types.maybeNull(types.string),
    // Step 1
    exchangeTxUUID: types.maybeNull(types.string),
    exchangeTxSourceShardLock: types.maybeNull(types.number),
    exchangeTxDestinationShardLock: types.maybeNull(types.number),
    // Step 2
    sourceShardFundsLockingTxHash: types.maybeNull(types.string),
    sourceShardMultiSigRedeemHex: types.maybeNull(types.string),
    destinationShardMultiSigRedeemHex: types.maybeNull(types.string),
    // Step 3
    isEALocked: types.optional(types.boolean, false),
    // Step 4
    exchangeAgentConfirmations: types.optional(types.number, 0),
    exchangeAgentConfirmationsRequired: types.optional(types.number, 12),
    rawCSTXHex: types.maybeNull(types.string),
    destinationShardFundsLockTxHash: types.maybeNull(types.string),
  })
  .views((self) => ({
    get step() {
      if (
        self.exchangeTxUUID !== null &&
        self.exchangeTxSourceShardLock !== null &&
        self.exchangeTxDestinationShardLock !== null &&
        self.sourceShardFundsLockingTxHash === null &&
        self.sourceShardMultiSigRedeemHex === null &&
        self.destinationShardMultiSigRedeemHex === null &&
        self.isEALocked === false &&
        self.rawCSTXHex === null &&
        self.destinationShardFundsLockTxHash === null
      ) {
        return 1
      }

      if (
        self.exchangeTxUUID !== null &&
        self.exchangeTxSourceShardLock !== null &&
        self.exchangeTxDestinationShardLock !== null &&
        self.sourceShardFundsLockingTxHash !== null &&
        self.sourceShardMultiSigRedeemHex !== null &&
        self.destinationShardMultiSigRedeemHex !== null &&
        self.isEALocked === false &&
        self.rawCSTXHex === null &&
        self.destinationShardFundsLockTxHash === null
      ) {
        return 2
      }

      if (
        self.exchangeTxUUID !== null &&
        self.exchangeTxSourceShardLock !== null &&
        self.exchangeTxDestinationShardLock !== null &&
        self.sourceShardFundsLockingTxHash !== null &&
        self.sourceShardMultiSigRedeemHex !== null &&
        self.destinationShardMultiSigRedeemHex !== null &&
        self.isEALocked === true &&
        self.rawCSTXHex === null &&
        self.destinationShardFundsLockTxHash === null
      ) {
        return 3
      }

      if (
        self.exchangeTxUUID !== null &&
        self.exchangeTxSourceShardLock !== null &&
        self.exchangeTxDestinationShardLock !== null &&
        self.sourceShardFundsLockingTxHash !== null &&
        self.sourceShardMultiSigRedeemHex !== null &&
        self.destinationShardMultiSigRedeemHex !== null &&
        self.isEALocked === true &&
        self.rawCSTXHex !== null &&
        self.destinationShardFundsLockTxHash !== null
      ) {
        return 4
      }

      return 0
    },
  }))

type HistoryCrossShardTempType = Instance<typeof HistoryCrossShardTempModel>
export interface HistoryCrossShardTemp extends HistoryCrossShardTempType {}
type HistoryCrossShardTempSnapshotType = SnapshotOut<typeof HistoryCrossShardTempModel>
export interface HistoryCrossShardTempSnapshot extends HistoryCrossShardTempSnapshotType {}
export const HistoryCrossShardTempDefaultModel = () =>
  types.optional(HistoryCrossShardTempModel, {})
