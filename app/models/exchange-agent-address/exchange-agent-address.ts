import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const ExchangeAgentAddressModel = types.model("ExchangeAgentAddress").props({
  ip: types.maybeNull(types.string),
  port: types.number,
  url: types.maybeNull(types.string),
  expires: types.string,
})

type ExchangeAgentAddressType = Instance<typeof ExchangeAgentAddressModel>
export interface ExchangeAgentAddress extends ExchangeAgentAddressType {}
type ExchangeAgentAddressSnapshotType = SnapshotOut<typeof ExchangeAgentAddressModel>
export interface ExchangeAgentAddressSnapshot extends ExchangeAgentAddressSnapshotType {}
export const createExchangeAgentAddressDefaultModel = () =>
  types.optional(ExchangeAgentAddressModel, {})
