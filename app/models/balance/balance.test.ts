import { BalanceModel } from "./balance"

test("can be created", () => {
  const instance = BalanceModel.create({
    shardID: 0,
    balance: 800000000,
  })

  expect(instance).toBeTruthy()
})
