import { Instance, SnapshotOut, types } from "mobx-state-tree"

export const BalanceModel = types
  .model("Balance")
  .props({
    shardID: types.number,
    availableBalance: types.optional(types.number, 0),
    lockedBalance: types.optional(types.number, 0),
  })
  .views((self) => ({
    get totalBalance(): number {
      return self.availableBalance + self.lockedBalance
    },
  }))

type BalanceType = Instance<typeof BalanceModel>
export interface Balance extends BalanceType {}
type BalanceSnapshotType = SnapshotOut<typeof BalanceModel>
export interface BalanceSnapshot extends BalanceSnapshotType {}
export const createBalanceDefaultModel = () => types.optional(BalanceModel, {})
