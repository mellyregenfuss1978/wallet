import { Instance, SnapshotOut, types } from "mobx-state-tree"

import { HistoryItemModel } from "../history-item/history-item"

export const HistoryModel = types.model("History").props({
  shardID: types.identifierNumber,
  items: types.map(HistoryItemModel),
})

type HistoryType = Instance<typeof HistoryModel>
export interface History extends HistoryType {}
type HistorySnapshotType = SnapshotOut<typeof HistoryModel>
export interface HistorySnapshot extends HistorySnapshotType {}
export const HistoryDefaultModel = () => types.optional(HistoryModel, {})
