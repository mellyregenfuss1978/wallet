import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import { SettingsScreen, SupportScreen } from "../../screens"
import { WalletBackupScreen } from "../../screens/wallet/wallet-backup/wallet-backup-screen"

export type SettingsNavigatorParamList = {
  settings: undefined
  walletBackup: undefined
  support: undefined
}

const Stack = createStackNavigator<SettingsNavigatorParamList>()

export const SettingsStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="settings" component={SettingsScreen} />
      <Stack.Screen name="walletBackup" component={WalletBackupScreen} />
      <Stack.Screen name="support" component={SupportScreen} />
    </Stack.Navigator>
  )
}
