import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import { BalanceScreen, LockedBalanceScreen } from "../../screens"
import { ReceiveStackNavigator } from "./receive-stack-navigator"
import { SendStackNavigator } from "./send-stack-navigator"
import { LockedBalanceScreenParams } from "../../screens/locked-balance"

export type BalanceNavigatorParamList = {
  balance: undefined
  lockedBalance: LockedBalanceScreenParams
  sendStack: undefined
  receiveStack: undefined
}

const Stack = createStackNavigator<BalanceNavigatorParamList>()

export const BalanceStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="balance" component={BalanceScreen} />
      <Stack.Screen name="lockedBalance" component={LockedBalanceScreen} />
      <Stack.Screen name="sendStack" component={SendStackNavigator} />
      <Stack.Screen name="receiveStack" component={ReceiveStackNavigator} />
    </Stack.Navigator>
  )
}
