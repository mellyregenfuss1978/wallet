import React from "react"

import { createStackNavigator } from "@react-navigation/stack"

import { OnboardingScreen } from "../../screens"
import { WalletStackNavigator } from "./wallet-stack-navigator"

export type OnboardingStackParamList = {
  onboarding: undefined
  walletStack: undefined
}

const Stack = createStackNavigator<OnboardingStackParamList>()

export const OnboardingStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="onboarding" component={OnboardingScreen} />
      <Stack.Screen name="walletStack" component={WalletStackNavigator} />
    </Stack.Navigator>
  )
}
