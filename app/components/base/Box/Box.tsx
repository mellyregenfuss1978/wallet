import { Box as NBBox } from "native-base"
import React, { FC } from "react"

import { BoxProps } from "./types"

export const Box: FC<BoxProps> = (props) => {
  return <NBBox {...props} />
}
