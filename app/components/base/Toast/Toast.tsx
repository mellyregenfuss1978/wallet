import { useTheme } from "native-base"
import React, { FC } from "react"
import { StyleSheet } from "react-native"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import RNToast from "react-native-toast-message"

import { Box } from "../Box"
import { Icon } from "../Icon"
import { HStack, VStack } from "../Stacks"
import { Text } from "../Text"

const ToastsContainer: FC = () => {
  const insets = useSafeAreaInsets()
  const { colors } = useTheme()

  const stylesByStatus = {
    success: {
      backgroundColor: colors.green[500],
      shadowColor: colors.green[500],
    },
    error: {
      backgroundColor: colors.red[500],
      shadowColor: colors.red[500],
    },
    info: {
      backgroundColor: colors.lightBlue[500],
      shadowColor: colors.lightBlue[500],
    },
  }

  const renderIcon = (status: "success" | "error" | "info") => {
    if (status === "success") return <Icon.Checkmark color="white" size={5} />

    if (status === "error") return <Icon.Info color="white" size={5} />

    return <Icon.Info color="white" size={5} />
  }

  const renderToast = (status: "success" | "error" | "info", internalState: any) => (
    <Box style={[styles.container, stylesByStatus[status], {}]} px="3" py="2" mx="2" my="1">
      <HStack flex={1} space="sm" alignItems="center" justifyContent="flex-start">
        {renderIcon(status)}
        <VStack flex={1} alignItems="flex-start" justifyContent="center">
          {internalState.text1 && (
            <Text text={internalState.text1} color="white" fontSize="sm" bold />
          )}
          {internalState.text2 && (
            <Text text={internalState.text2} color="whiteAlpha.900" fontSize="sm" />
          )}
        </VStack>
      </HStack>
    </Box>
  )

  const config = {
    info: (internalState: any) => renderToast("info", internalState),
    success: (internalState: any) => renderToast("success", internalState),
    error: (internalState: any) => renderToast("error", internalState),
  }

  return <RNToast config={config} topOffset={insets.top} ref={(ref) => RNToast.setRef(ref)} />
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    elevation: 2,
    flexDirection: "row",
    shadowOpacity: 0.05,
    shadowRadius: 100,
  },
})

export { RNToast as Toast, ToastsContainer }
