import { Flex as NBFlex } from "native-base"
import React, { FC } from "react"

import { FlexProps } from "./types"

export const Flex: FC<FlexProps> = (props) => {
  return <NBFlex {...props} />
}
