import { Button as NBButton } from "native-base"
import ButtonGroup from "native-base/src/components/primitives/Button/ButtonGroup"
import React, { FC } from "react"

import { translate } from "../../../i18n"
import { Flex } from "../Flex"
import { ButtonComponentType, ButtonProps } from "./types"

const ButtonMain: FC<ButtonProps> = (props) => {
  const {
    text,
    textTx,
    textTxOptions,
    children,
    size = "lg",
    startIcon,
    endIcon,
    flex,
    paddingX,
    onPress,
    ...rest
  } = props

  const content = text || (textTx && translate(textTx, textTxOptions)) || children

  return (
    <NBButton
      size={size}
      flex={flex}
      startIcon={
        startIcon && flex ? (
          <Flex flex={1} alignItems="flex-start">
            {startIcon}
          </Flex>
        ) : (
          startIcon
        )
      }
      endIcon={
        endIcon && flex ? (
          <Flex flex={1} alignItems="flex-end">
            {endIcon}
          </Flex>
        ) : (
          endIcon
        )
      }
      px={(endIcon || startIcon) && flex ? "2" : paddingX}
      onPress={onPress}
      {...rest}
    >
      {content}
    </NBButton>
  )
}

const ButtonTemp: any = ButtonMain
ButtonTemp.Group = ButtonGroup

// To add typings
const Button = ButtonTemp as ButtonComponentType

export { Button }
