import { Pressable as NBPressable } from "native-base"
import React, { FC, forwardRef } from "react"

import { PressableProps } from "./types"

const Pressable: FC<PressableProps> = forwardRef((props, ref) => {
  const { _pressed = { opacity: 0.8 }, ...rest } = props

  return <NBPressable _pressed={_pressed} ref={ref} {...rest} />
})

Pressable.displayName = "Pressable"

export { Pressable }
