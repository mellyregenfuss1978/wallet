import { Select as NBSelect } from "native-base"
import React, { FC } from "react"

import { translate } from "../../../i18n"
import { SelectComponentType, SelectProps } from "./types"

export const SelectMain: FC<SelectProps> = (props) => {
  const { placeholder, placeholderTx, placeholderTxOptions, ...rest } = props

  const translatedPlaceholder =
    placeholder || (placeholderTx ? translate(placeholderTx, placeholderTxOptions) : "")

  return <NBSelect placeholder={translatedPlaceholder} {...rest} />
}

const SelectTemp: any = SelectMain
SelectTemp.Item = NBSelect.Item

// To add typings
const Select = SelectTemp as SelectComponentType

export { Select }
