import { Text as NBText } from "native-base"
import React, { FC } from "react"
import { TextStyle } from "react-native"

import { translate } from "../../../i18n"
import { TextProps } from "./types"

export const Text: FC<TextProps> = (props) => {
  const {
    textTx,
    textTxOptions,
    text,
    align = "left",
    secondary = false,
    fontSize,
    fontWeight,
    lineHeight,
    color,
    children,
    ...rest
  } = props

  const content = text || (textTx ? translate(textTx, textTxOptions) : null) || children

  const styles: TextStyle[] = [
    {
      textAlign: align,
    },
  ]

  const getFontSize = () => {
    if (secondary) {
      return "sm"
    }

    return fontSize
  }

  const getFontWeight = () => {
    if (secondary && !fontWeight) {
      return 500
    }

    return fontWeight
  }

  const getLineHeight = () => {
    if (secondary && !lineHeight) {
      return "24px"
    }

    return lineHeight
  }

  const getLightTheme = () => {
    if (secondary && !color) {
      return { color: "primaryAlpha.500" }
    }

    return { color: color ?? "primary.500" }
  }

  const getDarkTheme = () => {
    if (secondary && !color) {
      return { color: "whiteAlpha.500" }
    }

    return { color: color ?? "white" }
  }

  return (
    <NBText
      fontSize={getFontSize()}
      lineHeight={getLineHeight()}
      fontWeight={getFontWeight()}
      _light={getLightTheme()}
      _dark={getDarkTheme()}
      style={styles}
      {...rest}
    >
      {content}
    </NBText>
  )
}
