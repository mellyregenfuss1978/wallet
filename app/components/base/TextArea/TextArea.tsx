import { TextArea as NBTextArea } from "native-base"
import React, { FC } from "react"

import { translate } from "../../../i18n"
import { TextAreaProps } from "./types"

export const TextArea: FC<TextAreaProps> = (props) => {
  const {
    placeholder,
    placeholderTx,
    placeholderTxOptions,
    color,
    placeholderTextColor,
    error = false,
    ...rest
  } = props

  const translatedPlaceholder =
    placeholder || (placeholderTx ? translate(placeholderTx, placeholderTxOptions) : "")

  return (
    <NBTextArea
      color={error ? "red.500" : color}
      placeholder={translatedPlaceholder}
      placeholderTextColor={error ? "red.400" : placeholderTextColor}
      {...rest}
    />
  )
}
