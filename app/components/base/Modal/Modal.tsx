import { useColorModeValue, useTheme, useToken } from "native-base"
import React, { useEffect, useRef, useState } from "react"
import { Keyboard, Platform, StyleSheet } from "react-native"
import { Modalize } from "react-native-modalize"
import { Portal } from "react-native-portalize"
import { useSafeAreaInsets } from "react-native-safe-area-context"

import { theme } from "../../../theme"
import { Box } from "../Box"
import { ModalProps } from "./types"

export const Modal = ({
  isVisible,
  adjustToContentHeight = true,
  children,
  HeaderComponent,
  FooterComponent,
  ...rest
}: ModalProps) => {
  const bottomSheetModalRef = useRef<Modalize>(null)
  const insets = useSafeAreaInsets()
  const { colors } = useTheme()

  const [keyboardIsOpen, setKeyboardIsOpen] = useState(false)

  const keyboardWillShow = () => setKeyboardIsOpen(true)
  const keyboardWillHide = () => setKeyboardIsOpen(false)

  useEffect(() => {
    isVisible ? bottomSheetModalRef?.current?.open() : bottomSheetModalRef?.current?.close()
  }, [isVisible])

  useEffect(() => {
    Keyboard.addListener("keyboardWillShow", keyboardWillShow)
    Keyboard.addListener("keyboardWillHide", keyboardWillHide)

    return () => {
      Keyboard.removeListener("keyboardWillShow", keyboardWillShow)
      Keyboard.removeListener("keyboardWillHide", keyboardWillHide)
    }
  }, [])

  return (
    <Portal>
      <Modalize
        ref={bottomSheetModalRef}
        adjustToContentHeight={adjustToContentHeight}
        modalStyle={[
          styles.root,
          { backgroundColor: useToken("colors", useColorModeValue("white", "darkGray.500")) },
        ]}
        childrenStyle={{
          paddingBottom:
            insets.bottom > 0 && !FooterComponent
              ? keyboardIsOpen
                ? theme.space[2]
                : insets.bottom
              : theme.space[0],
        }}
        overlayStyle={{
          backgroundColor: colors.darkGrayAlpha[500],
        }}
        handleStyle={{
          backgroundColor: useToken(
            "colors",
            useColorModeValue("primaryAlpha.200", "whiteAlpha.200"),
          ),
        }}
        handlePosition="inside"
        HeaderComponent={
          <Box px="6" pt={HeaderComponent ? "10" : "6"} pb="2">
            {HeaderComponent}
          </Box>
        }
        FooterComponent={
          FooterComponent && (
            <Box
              p="2"
              paddingBottom={
                insets.bottom > 0
                  ? keyboardIsOpen
                    ? theme.space[2]
                    : insets.bottom
                  : Platform.OS === "ios"
                  ? theme.space[2]
                  : 0
              }
            >
              {FooterComponent}
            </Box>
          )
        }
        {...rest}
      >
        {children && (
          <Box px="6" pb="6" pt="4">
            {children}
          </Box>
        )}
      </Modalize>
    </Portal>
  )
}

const styles = StyleSheet.create({
  root: {
    borderTopLeftRadius: theme.radii.md,
    borderTopRightRadius: theme.radii.md,
  },
})
