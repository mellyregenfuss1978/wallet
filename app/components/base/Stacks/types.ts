import { IHStackProps } from "native-base/src/components/primitives/Stack/HStack"
import { IVStackProps } from "native-base/src/components/primitives/Stack/VStack"

export type HStackProps = IHStackProps

export type VStackProps = IVStackProps
