import React, { FC } from "react"
import { StyleSheet } from "react-native"

import { useNavigation } from "@react-navigation/native"

import { translate } from "../../../i18n"
import { sizing } from "../../../theme"
import { Box } from "../Box"
import { Flex } from "../Flex"
import { Icon } from "../Icon"
import { IconButton } from "../IconButton"
import { HStack } from "../Stacks"
import { Text } from "../Text"
import { HeaderProps } from "./types"

export const Header: FC<HeaderProps> = ({
  title,
  titleTx,
  titleTxOptions,
  hasBackAction = true,
  rightAction,
}) => {
  const navigation = useNavigation()

  const translatedTitle = title || (titleTx ? translate(titleTx, titleTxOptions) : "")

  const onBackPress = () => navigation.canGoBack() && navigation.goBack()

  return (
    <Box style={styles.root}>
      <HStack flex={1} justifyContent="space-between" alignItems="center">
        {hasBackAction && (
          <IconButton
            testID="navigation-back-button"
            variant="unstyled"
            icon={<Icon.ChevronLeft />}
            _pressed={{ opacity: 0.8 }}
            onPress={onBackPress}
          />
        )}
        <Flex flex={1} pl={!hasBackAction ? 6 : "0"}>
          <Text
            _light={{ color: "primary.500" }}
            _dark={{ color: "white" }}
            fontSize={16}
            textTransform="uppercase"
          >
            {translatedTitle}
          </Text>
        </Flex>
        {rightAction && <Box pr={2}>{rightAction}</Box>}
      </HStack>
    </Box>
  )
}

const styles = StyleSheet.create({
  root: {
    height: sizing.headerHeight,
  },
})
