import { TxKeyPath } from "../../../i18n/i18n-types"

export type HeaderProps = {
  title?: string
  titleTx?: TxKeyPath
  titleTxOptions?: I18n.TranslateOptions

  hasBackAction?: boolean

  rightAction?: React.ReactElement
}
