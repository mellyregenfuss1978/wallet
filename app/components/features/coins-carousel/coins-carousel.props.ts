import { Coin } from "../../../models"

export type CoinsCarouselProps = {
  coins: Coin[]
  initialIndex: number
  onSnapToItem?(item: Coin): void
  onLockedBalancePress?(item: Coin): void
  onScrollBegin?(): void
  onScrollEnd?(): void
}
