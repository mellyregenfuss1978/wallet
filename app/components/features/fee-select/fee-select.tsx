import { observer } from "mobx-react-lite"
import { useColorModeValue } from "native-base"
import React, { FC } from "react"

import { TxKeyPath } from "../../../i18n"
import { useStores } from "../../../models"
import { CoinsFormatter } from "../../../utils/formatters"
import { Box, Flex, HStack, Pressable, Text, VStack } from "../../base"
import { FeeSelectProps } from "./fee-select.props"

export const FeeSelect: FC<FeeSelectProps> = observer((props) => {
  const { fees, value, coin, onChange } = props
  const { transactionStore } = useStores()

  return (
    <Box rounded="xl" overflow="hidden">
      <HStack alignItems="center" justifyContent="center">
        {fees.map((fee, index) => {
          const _feeValue = transactionStore.feeByName(fee.name)?.value
          const feeValue = _feeValue ? CoinsFormatter.toBiggestUnits(coin.name, _feeValue) : "N/A"

          return (
            <Flex key={`fee-${index}`} flex={1}>
              <Pressable onPress={() => onChange(fee)}>
                <Box
                  py="4"
                  bgColor={
                    fee.name === value.name
                      ? useColorModeValue("primaryAlpha.50", "whiteAlpha.50")
                      : useColorModeValue("offWhite", "darkGray.500")
                  }
                >
                  <VStack space="sm" alignItems="center">
                    <Text
                      textTx={
                        `labels.fee${
                          fee.name.charAt(0).toUpperCase() + fee.name.slice(1)
                        }` as TxKeyPath
                      }
                    />
                    <HStack alignItems="center">
                      <Text text={`${feeValue} ${coin.title}`} secondary />
                    </HStack>
                  </VStack>
                </Box>
              </Pressable>
            </Flex>
          )
        })}
      </HStack>
    </Box>
  )
})
