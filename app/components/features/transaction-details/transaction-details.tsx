import React, { FC } from "react"

import Clipboard from "@react-native-community/clipboard"
import numeral from "numeral"
import { translate, TxKeyPath } from "../../../i18n"
import { useStores } from "../../../models"
import { CoinsFormatter } from "../../../utils/formatters"
import { Flex, HStack, Icon, IconButton, Link, Pressable, Text, Toast, VStack } from "../../base"
import { TransactionDetailsProps } from "./transaction-details.props"
import { Popover } from "native-base"

export const TransactionDetails: FC<TransactionDetailsProps> = (props) => {
  const {
    coin,
    sourceAmount,
    destinationAmount,
    address,
    sourceShardID,
    destinationShardID,
    transactionFee,
    hash,
    confirmations,
    blockNumber,
    exchangeAgentFee,
    exchangeAgentUrl,
    direction = "out",
    lockTransactionHash,
  } = props
  const { networkName } = useStores()

  const _sourceAmount = CoinsFormatter.toBiggestUnits(coin.name, sourceAmount)
  const formattedSourceAmount =
    _sourceAmount > 10000 ? numeral(_sourceAmount).format("0.00a") : _sourceAmount

  const _destinationAmount = CoinsFormatter.toBiggestUnits(coin.name, destinationAmount)
  const formattedDestinationAmount =
    _destinationAmount > 10000 ? numeral(_destinationAmount).format("0.00a") : _destinationAmount

  const addressUrl = `https://explore.jax.net/shards/addresses/${address}?network=${networkName}`
  const transactionUrl =
    coin.name === "btc"
      ? `https://www.blockchain.com/en/search?search=${hash}`
      : `https://explore.jax.net/shards/${sourceShardID}/transactions/${hash}?network=${networkName}`
  const lockTransactionUrl = `https://explore.jax.net/shards/${
    direction === "out" ? sourceShardID : destinationShardID
  }/transactions/${lockTransactionHash}?network=${networkName}`

  const isCrossShardTransaction =
    destinationShardID !== null &&
    destinationShardID !== undefined &&
    sourceShardID !== destinationShardID

  const onCopy = (text: string) => {
    Clipboard.setString(text)
    Toast.show({
      type: "info",
      text1: translate("notifications.copied"),
      text2: text,
    })
  }

  const renderLabel = (textTx: TxKeyPath) => (
    <Text
      textTx={textTx}
      fontSize="sm"
      _light={{ color: "primaryAlpha.500" }}
      _dark={{ color: "whiteAlpha.500" }}
    />
  )

  const renderRegularTransactionDetails = () => (
    <VStack space="lg">
      <HStack>
        <VStack flex={1}>
          <HStack space={1}>
            {_sourceAmount > 10000 ? (
              <Popover
                trigger={(triggerProps) => {
                  return (
                    <Pressable {...triggerProps}>
                      <Text text={`${formattedSourceAmount}`} />
                    </Pressable>
                  )
                }}
              >
                <Popover.Content zIndex={9999}>
                  <Popover.Arrow />
                  <Popover.Body>
                    <Text text={`${sourceAmount}`} fontSize={14} />
                  </Popover.Body>
                </Popover.Content>
              </Popover>
            ) : (
              <Text text={`${_sourceAmount}`} />
            )}
            <Text text={coin.title} color={`coins.${coin.name}.500`} />
          </HStack>
          {renderLabel("labels.amount")}
        </VStack>
        {coin.name === "jax" && (
          <VStack flex={1}>
            <Text text={`${translate("labels.shard")} ${sourceShardID}`} />
            {renderLabel("labels.shard")}
          </VStack>
        )}
      </HStack>

      <HStack>
        <VStack>
          <HStack space={1}>
            <Text text={`${CoinsFormatter.toBiggestUnits(coin.name, transactionFee)}`} />
            <Text text={coin.title} color={`coins.${coin.name}.500`} />
          </HStack>

          {renderLabel("labels.fee")}
        </VStack>
      </HStack>
    </VStack>
  )

  const renderCrossShardTransactionDetails = () => (
    <VStack space="lg">
      <HStack>
        <VStack flex={1}>
          <HStack space={1}>
            {_sourceAmount > 10000 ? (
              <Popover
                trigger={(triggerProps) => {
                  return (
                    <Pressable {...triggerProps}>
                      <Text text={`${formattedSourceAmount}`} />
                    </Pressable>
                  )
                }}
              >
                <Popover.Content zIndex={9999}>
                  <Popover.Arrow />
                  <Popover.Body>
                    <Text text={`${sourceAmount}`} fontSize={14} />
                  </Popover.Body>
                </Popover.Content>
              </Popover>
            ) : (
              <Text text={`${_sourceAmount}`} />
            )}
            <Text text={coin.title} color={`coins.${coin.name}.500`} />
          </HStack>
          {renderLabel("labels.willBeSent")}
        </VStack>
        <VStack flex={1}>
          <Text text={`${translate("labels.shard")} ${sourceShardID}`} />
          {renderLabel("labels.sourceShard")}
        </VStack>
      </HStack>

      <HStack>
        <VStack flex={1}>
          <HStack space={1}>
            {_destinationAmount > 10000 ? (
              <Popover
                trigger={(triggerProps) => {
                  return (
                    <Pressable {...triggerProps}>
                      <Text text={`${formattedDestinationAmount}`} />
                    </Pressable>
                  )
                }}
              >
                <Popover.Content zIndex={9999}>
                  <Popover.Arrow />
                  <Popover.Body>
                    <Text text={`${destinationAmount}`} fontSize={14} />
                  </Popover.Body>
                </Popover.Content>
              </Popover>
            ) : (
              <Text text={`${_destinationAmount}`} />
            )}
            <Text text={coin.title} color={`coins.${coin.name}.500`} />
          </HStack>

          {renderLabel("labels.willBeReceived")}
        </VStack>
        <VStack flex={1}>
          <Text text={`${translate("labels.shard")} ${destinationShardID}`} />
          {renderLabel("labels.destinationShard")}
        </VStack>
      </HStack>

      <HStack>
        <VStack flex={1}>
          <HStack alignItems="center" space={2}>
            <HStack space={1}>
              <Text
                text={`${CoinsFormatter.toBiggestUnits(
                  coin.name,
                  transactionFee + exchangeAgentFee,
                )}`}
              />
              <Text text={coin.title} color={`coins.${coin.name}.500`} />
            </HStack>
            <Popover
              trigger={(triggerProps) => {
                return (
                  <Pressable {...triggerProps} alignSelf="center">
                    <Icon.Info size={18} />
                  </Pressable>
                )
              }}
            >
              <Popover.Content>
                <Popover.Arrow />
                <Popover.Body>
                  <VStack>
                    <Text
                      fontSize={14}
                      text={`${CoinsFormatter.toBiggestUnits(coin.name, transactionFee)} ${
                        coin.title
                      } — ${translate("labels.networkFee")}`}
                    />
                    <Text
                      fontSize={14}
                      text={`${CoinsFormatter.toBiggestUnits(coin.name, exchangeAgentFee)} ${
                        coin.title
                      } — ${translate("labels.exchangeAgentFee")}`}
                    />
                  </VStack>
                </Popover.Body>
              </Popover.Content>
            </Popover>
          </HStack>
          {renderLabel("labels.fee")}
        </VStack>
      </HStack>
    </VStack>
  )

  return (
    <VStack space="lg">
      <VStack>
        <HStack alignItems="center">
          <Flex flex={1}>
            <Link text={address} url={addressUrl} />
          </Flex>
          <IconButton
            variant="unstyled"
            icon={<Icon.Copy size="sm" />}
            size="sm"
            onPress={() => onCopy(address)}
          />
        </HStack>
        {renderLabel(`labels.${direction === "in" ? "sender" : "receiver"}`)}
      </VStack>

      {isCrossShardTransaction
        ? renderCrossShardTransactionDetails()
        : renderRegularTransactionDetails()}

      {exchangeAgentUrl && (
        <VStack>
          <HStack alignItems="center">
            <Flex flex={1}>
              <Text text={exchangeAgentUrl} />
            </Flex>
            <IconButton
              variant="unstyled"
              size="sm"
              icon={<Icon.Copy size="sm" />}
              onPress={() => onCopy(exchangeAgentUrl)}
            />
          </HStack>
          {renderLabel("labels.exchangeAgent")}
        </VStack>
      )}

      {confirmations !== null && blockNumber !== undefined && (
        <HStack>
          <VStack flex={1}>
            <Text text={`${confirmations}`} />
            {renderLabel("labels.confirmations")}
          </VStack>
          <VStack flex={1}>
            <Text text={`${blockNumber ?? "Mempool"}`} />
            {renderLabel("labels.blockNumber")}
          </VStack>
        </HStack>
      )}

      {hash && (
        <VStack>
          <HStack alignItems="center">
            <Flex flex={1}>
              <Link text={hash} url={transactionUrl} />
            </Flex>
            <IconButton
              variant="unstyled"
              icon={<Icon.Copy size="sm" />}
              size="sm"
              onPress={() => onCopy(transactionUrl)}
            />
          </HStack>
          {renderLabel("labels.hash")}
        </VStack>
      )}

      {lockTransactionHash && (
        <VStack>
          <HStack alignItems="center">
            <Flex flex={1}>
              <Link text={lockTransactionHash} url={lockTransactionUrl} />
            </Flex>
            <IconButton
              variant="unstyled"
              icon={<Icon.Copy size="sm" />}
              size="sm"
              onPress={() => onCopy(lockTransactionUrl)}
            />
          </HStack>
          {renderLabel("labels.lockTransactionHash")}
        </VStack>
      )}
    </VStack>
  )
}
