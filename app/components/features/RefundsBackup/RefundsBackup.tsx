import React, { FC, useEffect, useState } from "react"
import { Share } from "react-native"

import Clipboard from "@react-native-community/clipboard"

import { translate } from "../../../i18n"
import { useStores } from "../../../models"
import { CoinsFormatter } from "../../../utils/formatters"
import { Button, HStack, Icon, Input, Text, Toast, VStack } from "../../base"

export const RefundsBackup: FC = () => {
  const { historyStore } = useStores()
  const { localHistory } = historyStore

  const [refundsCount, setRefundsCount] = useState(0)
  const [refundsString, setRefundsString] = useState("")

  const onCopy = () => {
    Clipboard.setString(refundsString)
    Toast.show({
      type: "info",
      text1: translate("notifications.copied"),
    })
  }

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: refundsString,
      })
      if (result.action === Share.sharedAction) {
        Toast.show({
          type: "info",
          text1: translate("notifications.shared"),
        })
      }
    } catch (error) {}
  }

  useEffect(() => {
    let count = 0
    let formattedRefundString = ""

    localHistory.forEach((history) => {
      history.items.forEach((historyItem) => {
        if (historyItem.isCrossShard && historyItem.refundTransactionHex) {
          count++

          const refundString = `${formattedRefundString && "\n\n"}${
            historyItem.timestamp
          }\n${CoinsFormatter.toBiggestUnits("jax", historyItem.sourceAmount)} JAX\n${
            historyItem.address
          }\nShard ${historyItem.sourceShardID} -> Shard ${historyItem.destinationShardID}\n${
            historyItem.refundTransactionHex
          }`

          formattedRefundString += refundString
        }
      })
    })

    setRefundsString(formattedRefundString)
    setRefundsCount(count)
  }, [])

  return (
    <VStack space="lg">
      <HStack justifyContent="center">
        <Text textTx="texts.backupRefundsConfirm" textTxOptions={{ refundsCount }} align="center" />
      </HStack>

      <VStack space="md">
        <Input editable={false} value={refundsString} size="lg" height={120} multiline />
        <HStack space="xs">
          <Button
            flex={1}
            textTx="buttons.copy"
            variant="outline"
            borderRadius="md"
            endIcon={<Icon.Copy />}
            onPress={onCopy}
          />
          <Button
            flex={1}
            textTx="buttons.share"
            variant="outline"
            borderRadius="md"
            endIcon={<Icon.Share />}
            onPress={onShare}
          />
        </HStack>
      </VStack>
    </VStack>
  )
}
