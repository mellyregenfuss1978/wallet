import React, { FC, useState } from "react"

import { translate } from "../../../i18n"
import { useStores } from "../../../models"
import { Button, Toast } from "../../base"
import { RefundButtonProps } from "./refund-button.props"

export const RefundButton: FC<RefundButtonProps> = (props) => {
  const { shardID, transactionHex, onRefund, onError, ...rest } = props

  const { transactionStore } = useStores()
  const [loading, setLoading] = useState(false)

  const refund = async () => {
    if (!transactionHex) {
      onRefund(null)
      return
    }

    setLoading(true)

    const [data, error] = await transactionStore.publish(shardID, transactionHex)

    if (error) {
      onError && onError(error.message)

      if (error.source && error.source.code === 108) {
        Toast.show({
          type: "error",
          text1: translate("notifications.refundNotReadyTitle"),
          text2: translate("notifications.refundNotReadyDescription"),
        })
      } else {
        Toast.show({
          type: "error",
          text1: translate("notifications.error"),
          text2: `${error.message}`,
        })
      }

      setLoading(false)
      return
    }

    onRefund(data)
    setLoading(false)
  }

  return <Button textTx="buttons.refund" isLoading={loading} onPress={refund} {...rest} />
}
