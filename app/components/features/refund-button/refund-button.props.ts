import { IButtonProps } from "native-base"

export type RefundButtonProps = IButtonProps & {
  shardID: number
  transactionHex?: string
  onRefund(hash?: string): void
  onError?: (message: string) => void
}
