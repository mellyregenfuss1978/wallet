import { observer, Observer } from "mobx-react-lite"
import React, { useState } from "react"
import { deleteUserPinCode } from "@haskkor/react-native-pincode"
import { StackActions, useNavigation } from "@react-navigation/native"
import DeviceInfo from "react-native-device-info"
import {
  Button,
  Card,
  Divider,
  Header,
  HStack,
  Icon,
  Modal,
  Pressable,
  Screen,
  ScreenContent,
  Spinner,
  Switch,
  Text,
  VStack,
} from "../../components/base"
import { RefundsBackup } from "../../components/features"
import { useTabBarVisible } from "../../hooks"
import { useStores } from "../../models"

export const SettingsScreen = observer(() => {
  const navigation = useNavigation()
  const rootStore = useStores()
  const { walletStore, historyStore } = rootStore
  const { localHistory } = historyStore

  const [networkTypeIsChanging, setNetworkTypeIsChanging] = useState(false)
  const [logoutConfirmationModalIsVisible, setLogoutConfirmationModalIsVisible] = useState(false)
  const [changeNetworkModalIsVisible, setChangeNetworkModalIsVisible] = useState(false)

  const hasUnsavedRefunds = () => {
    let unsavedRefundsCount = 0

    localHistory.forEach((history) => {
      history.items.forEach((historyItem) => {
        if (historyItem.isCrossShard) {
          unsavedRefundsCount++
        }
      })
    })

    return unsavedRefundsCount > 0
  }

  const navigateToWalletBackupScreen = () => navigation.navigate("walletBackup")
  const navigateToSupportScreen = () => navigation.navigate("support")

  const onChangeNetworkType = async () => {
    setNetworkTypeIsChanging(true)

    try {
      await rootStore.changeNetworkType(rootStore.networkName === "testnet" ? "mainnet" : "testnet")
    } catch {
      setNetworkTypeIsChanging(false)
    }

    setNetworkTypeIsChanging(false)
    setChangeNetworkModalIsVisible(false)

    navigation.reset({
      routes: [
        {
          name: "balance",
          state: {
            index: 0,
            routes: [],
          },
        },
      ],
    })
  }

  const onTogglePinCode = () => {
    if (rootStore.pinCodeScreenEnabled) {
      deleteUserPinCode()
      rootStore.disablePinCodeScreen()
    } else {
      rootStore.enablePinCodeScreen()
      rootStore.showPinCodeScreen()
    }
  }

  const onLogout = () => {
    deleteUserPinCode()
    rootStore.reset()

    navigation.dispatch(
      StackActions.replace("onboardingStack", {
        screen: "onboarding",
      }),
    )
  }

  const renderChangeNetworkConfirmationModal = () => (
    <Modal
      isVisible={changeNetworkModalIsVisible}
      FooterComponent={
        <HStack space={2}>
          <Button
            flex={1}
            textTx="buttons.cancel"
            variant="ghost"
            onPress={() => setChangeNetworkModalIsVisible(false)}
          />
          <Button
            flex={1}
            textTx="buttons.confirm"
            isLoading={networkTypeIsChanging}
            onPress={onChangeNetworkType}
          />
        </HStack>
      }
      onClosed={() => setChangeNetworkModalIsVisible(false)}
    >
      {hasUnsavedRefunds() ? (
        <RefundsBackup />
      ) : (
        <HStack justifyContent="center">
          <Text
            textTx="texts.changeNetworkConfirm"
            textTxOptions={{
              networkType: rootStore.networkName === "mainnet" ? "TestNet" : "MainNet",
            }}
            align="center"
          />
        </HStack>
      )}
    </Modal>
  )

  const renderLogoutConfirmationModal = () => (
    <Modal
      isVisible={logoutConfirmationModalIsVisible}
      FooterComponent={
        <HStack space={2}>
          <Button
            flex={1}
            textTx="buttons.cancel"
            variant="ghost"
            onPress={() => setLogoutConfirmationModalIsVisible(false)}
          />
          <Button flex={1} textTx="buttons.logout" onPress={onLogout} />
        </HStack>
      }
      onClosed={() => setLogoutConfirmationModalIsVisible(false)}
    >
      {hasUnsavedRefunds() ? (
        <RefundsBackup />
      ) : (
        <HStack justifyContent="center">
          <Text textTx="texts.logoutConfirm" align="center" />
        </HStack>
      )}
    </Modal>
  )

  useTabBarVisible(true)

  return (
    <Screen header={<Header titleTx="titles.settings" hasBackAction={false} />}>
      <ScreenContent>
        {renderLogoutConfirmationModal()}
        {renderChangeNetworkConfirmationModal()}

        <VStack space="xs">
          <Card pb="0">
            <Text
              textTx="labels.security"
              textTransform="uppercase"
              _light={{ color: "primaryAlpha.500" }}
              _dark={{ color: "whiteAlpha.500" }}
            />
            {walletStore.mnemonic && (
              <>
                <Pressable onPress={navigateToWalletBackupScreen}>
                  <HStack py="5" alignItems="center" justifyContent="space-between">
                    <Text textTx="buttons.backupPhrase" />
                    <Icon.ChevronRight
                      _light={{ color: "primaryAlpha.200" }}
                      _dark={{ color: "whiteAlpha.200" }}
                    />
                  </HStack>
                </Pressable>
                <Divider />
              </>
            )}
            <HStack py="5" alignItems="center" justifyContent="space-between">
              <Text textTx="labels.pinCode" />
              <Switch value={rootStore.pinCodeScreenEnabled} onToggle={onTogglePinCode} />
            </HStack>
          </Card>

          <Card pb="0">
            <Text
              textTx="labels.general"
              textTransform="uppercase"
              _light={{ color: "primaryAlpha.500" }}
              _dark={{ color: "whiteAlpha.500" }}
            />

            <Pressable onPress={() => setChangeNetworkModalIsVisible(true)}>
              <HStack py="5" alignItems="center" justifyContent="space-between">
                <Text textTx="labels.network" />
                {networkTypeIsChanging ? (
                  <Spinner />
                ) : (
                  <Observer>
                    {() => (
                      <Text text={rootStore.networkName} secondary textTransform="capitalize" />
                    )}
                  </Observer>
                )}
              </HStack>
            </Pressable>
            {/* <Divider />
            <Pressable onPress={navigateToSupportScreen}>
              <HStack py="5" alignItems="center" justifyContent="space-between">
                <Text textTx="buttons.support" />
                <Icon.ChevronRight
                  _light={{ color: "primaryAlpha.200" }}
                  _dark={{ color: "whiteAlpha.200" }}
                />
              </HStack>
            </Pressable> */}
          </Card>

          <Card py="0">
            <Pressable onPress={() => setLogoutConfirmationModalIsVisible(true)}>
              <HStack py="5" alignItems="center" justifyContent="space-between">
                <Text textTx="buttons.logout" />
                <Icon.Logout
                  size={5}
                  _light={{ color: "primaryAlpha.200" }}
                  _dark={{ color: "whiteAlpha.200" }}
                />
              </HStack>
            </Pressable>
          </Card>

          <HStack justifyContent="center">
            <Text
              text={`v${DeviceInfo.getVersion()} (${DeviceInfo.getBuildNumber()})`}
              secondary
              opacity={40}
            />
          </HStack>
        </VStack>
      </ScreenContent>
    </Screen>
  )
})
