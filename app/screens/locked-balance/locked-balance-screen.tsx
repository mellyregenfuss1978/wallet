import React, { useEffect, useState } from "react"

import { useRoute } from "@react-navigation/native"

import {
  Box,
  Card,
  Header,
  HStack,
  Icon,
  RefreshControl,
  Screen,
  ScreenFlatListContent,
  Spinner,
  Text,
  Toast,
  VStack,
} from "../../components/base"
import { useTabBarVisible } from "../../hooks"
import { useStores } from "../../models"
import { LockedBalanceScreenRouteProps } from "./locked-balance-screen.props"
import { StyleSheet } from "react-native"
import moment from "moment"
import { theme } from "../../theme"
import { CoinsFormatter } from "../../utils/formatters"
import { LockedUTXO } from "../../models/locked-utxo/locked-utxo"
import { observer } from "mobx-react-lite"
import { translate } from "../../i18n"

export const LockedBalanceScreen = observer(() => {
  const route = useRoute<LockedBalanceScreenRouteProps>()
  const { shardIDs } = route.params ?? {}

  const rootStore = useStores()
  const { walletStore, lockedUTXOsStore } = rootStore
  const { address } = walletStore
  const { lockedUTXOsByDate } = lockedUTXOsStore

  const [refreshing, setRefreshing] = useState(false)
  const [lockedBalanceIsLoadingMore, setLockedBalanceIsLoadingMore] = useState(false)

  const getLockedBalance = async (refresh: boolean, dayClosed: boolean, offset = 0) => {
    return lockedUTXOsStore
      .getLockedUTXOs(address, shardIDs, dayClosed, refresh, offset)
      .then((response) => {
        if (lockedUTXOsStore.isError) {
          Toast.show({
            type: "error",
            text1: translate("notifications.error"),
            text2: `${lockedUTXOsStore.error.message}`,
          })
        } else if (
          refresh &&
          response.kind === "ok" &&
          !response?.data?.utxos.length &&
          response?.data?.nearestDates?.previous
        ) {
          return lockedUTXOsStore.getLockedUTXOs(address, shardIDs, true, false)
        }
      })
  }

  const onRefresh = async () => {
    setRefreshing(true)
    lockedUTXOsStore.reset()
    await getLockedBalance(true, true, 0)
    setRefreshing(false)
  }

  const onLoadMore = async () => {
    if (lockedBalanceIsLoadingMore || lockedUTXOsStore.loadedAllPreviousItems) return
    setLockedBalanceIsLoadingMore(true)

    const sections = lockedUTXOsByDate(shardIDs)

    const isSameDay = moment(sections[sections.length - 1].title).isSameOrBefore(
      lockedUTXOsStore.nearestPreviousDate,
    )

    const dayClosed =
      !isSameDay && sections[sections.length - 1].items.length >= lockedUTXOsStore.stats.totalCount

    await getLockedBalance(
      false,
      dayClosed,
      dayClosed ? 0 : sections[sections.length - 1].items.length,
    )

    setLockedBalanceIsLoadingMore(false)
  }

  const formatHistorySectionDate = (date: string) => {
    const momentDate = moment(date)
    const formattedDate = momentDate.format(
      momentDate.year() === moment().year() ? "D MMM" : "D MMM YYYY",
    )

    return formattedDate
  }

  const renderLockedBalanceItem = ({ item: section, index }) => (
    <Box my="1" key={`list-section-${index}`} style={styles.listSection}>
      <Card padding={4}>
        <VStack space={4}>
          <Text text={formatHistorySectionDate(section.title)} secondary />

          <VStack space={5}>
            {section.items.map((item: LockedUTXO, index: number) => (
              <HStack key={`list-item-${item.hash}-${index}`} alignItems="stretch" space={2}>
                <Box
                  p="3"
                  rounded="lg"
                  borderWidth={1}
                  alignItems="center"
                  justifyContent="center"
                  _light={{ borderColor: "primaryAlpha.50" }}
                  _dark={{ borderColor: "whiteAlpha.100" }}
                >
                  <Icon.Lock color={`coins.jxn.500`} />
                </Box>

                <VStack flex={1} justifyContent="space-around">
                  <HStack alignItems="center">
                    <HStack flex={1} space={1}>
                      <Text text={`${CoinsFormatter.toBiggestUnits("jxn", item.amount)}`} />
                      <Text text={"JXN"} color={`coins.jxn.500`} fontWeight={400} />
                    </HStack>
                    <Text textTx={`labels.statusLocked`} secondary fontWeight={400} />
                  </HStack>
                  <HStack justifyContent="space-between">
                    <Text
                      textTx="texts.till"
                      textTxOptions={{
                        date: moment.unix(item.unlockingTimestamp).format("LLL"),
                      }}
                      secondary
                      fontWeight={400}
                    />
                    <Text text={`#${item.unlockingBlock}`} secondary fontWeight={400} />
                  </HStack>
                </VStack>
              </HStack>
            ))}
          </VStack>

          {lockedUTXOsStore.stats.totalCount > section.items.length &&
            index === lockedUTXOsByDate(shardIDs).length - 1 && (
              <VStack mt={2}>
                <Text
                  align="center"
                  textTx="texts.loadedLockedUTXOsCountFrom"
                  textTxOptions={{
                    count: section.items.length,
                    totalCount: lockedUTXOsStore.stats.totalCount,
                  }}
                  secondary
                />
                <Text align="center" textTx="texts.scrollDownToLoadMore" secondary />
              </VStack>
            )}
        </VStack>
      </Card>
    </Box>
  )

  useEffect(() => {
    lockedUTXOsStore.reset()
    getLockedBalance(true, true, 0)

    return () => {
      lockedUTXOsStore.reset()
    }
  }, [])

  useTabBarVisible(false)

  return (
    <Screen header={<Header titleTx="titles.lockedBalance" hasBackAction={true} />} unsafeBottom>
      <ScreenFlatListContent
        data={refreshing ? [] : lockedUTXOsByDate(shardIDs)}
        paddingLess
        contentContainerStyle={styles.list}
        ListEmptyComponent={
          <HStack mx="2" alignContent="center" justifyContent="center" height="56px">
            {lockedUTXOsStore.isLoading ? (
              <Spinner />
            ) : (
              <Card flex={1}>
                <Text textTx="texts.emptyOperations" align="center" secondary />
              </Card>
            )}
          </HStack>
        }
        ListFooterComponent={
          <HStack mx="2" my="1" alignContent="center" justifyContent="center" height="56px">
            {lockedBalanceIsLoadingMore ||
            (!refreshing &&
              !lockedBalanceIsLoadingMore &&
              lockedUTXOsStore.isLoading &&
              lockedUTXOsByDate(shardIDs).length > 0) ? (
              <Spinner />
            ) : lockedUTXOsStore.loadedAllPreviousItems &&
              !lockedBalanceIsLoadingMore &&
              !lockedUTXOsStore.isLoading &&
              lockedUTXOsByDate(shardIDs).length > 0 ? (
              <Card flex={1}>
                <Text textTx="texts.loadedAllTransactions" align="center" secondary />
              </Card>
            ) : (
              <></>
            )}
          </HStack>
        }
        renderItem={renderLockedBalanceItem}
        keyExtractor={(_, index) => `history-section-${index.toString()}`}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            titleTx="texts.refreshing"
          />
        }
        onEndReachedThreshold={0.5}
        onEndReached={onLoadMore}
      />
    </Screen>
  )
})

const styles = StyleSheet.create({
  list: {
    paddingBottom: 96,
  },
  listSection: {
    paddingHorizontal: theme.space[2],
  },
})
