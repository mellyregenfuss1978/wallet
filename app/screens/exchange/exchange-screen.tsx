import React from "react"

import { Header, Screen, ScreenScrollViewContent, Text } from "../../components/base"
import { useTabBarVisible } from "../../hooks"

export const ExchangeScreen = () => {
  useTabBarVisible(true)

  return (
    <Screen header={<Header title="Exchange" hasBackAction={false} />}>
      <ScreenScrollViewContent>
        <Text text="Coming soon..." align="center" />
      </ScreenScrollViewContent>
    </Screen>
  )
}
