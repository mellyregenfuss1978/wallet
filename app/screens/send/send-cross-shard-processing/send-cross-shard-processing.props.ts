import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { SendNavigatorParamList } from "../../../navigators/stacks"

export type SentCrossShardProcessingScreenRouteProps = RouteProp<
  SendNavigatorParamList,
  "sendCrossShardProcessing"
>
export type SentCrossShardProcessingScreenNavigationProps = StackNavigationProp<
  SendNavigatorParamList,
  "sendCrossShardProcessing"
>
