import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { SendNavigatorParamList } from "../../../navigators/stacks"

export type SentSuccessfullyScreenRouteProps = RouteProp<SendNavigatorParamList, "sentSuccessfully">
export type SentSuccessfullyScreenNavigationProps = StackNavigationProp<
  SendNavigatorParamList,
  "sentSuccessfully"
>
