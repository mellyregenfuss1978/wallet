import { SendCrossShardProcessingScreenParams } from "../send-cross-shard-processing"

export type SentSuccessfullyScreenParams = SendCrossShardProcessingScreenParams & {
  address: string
  sourceAmount: number
  destinationAmount: number
  transactionFee: number
  exchangeAgentFee: number
  exchangeAgentUrl: string
}
