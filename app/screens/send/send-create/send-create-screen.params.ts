export type SendCreateScreenParams = {
  coinName: string
  address?: string
  amount?: number
  shardID?: number
}
