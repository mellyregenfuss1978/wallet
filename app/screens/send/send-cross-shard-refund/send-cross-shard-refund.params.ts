export type SendCrossShardRefundScreenParams = {
  shardID: number
  amount: number
  transactionFee: number
}
