import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { SendNavigatorParamList } from "../../../navigators/stacks"

export type SentCrossShardRefundScreenRouteProps = RouteProp<
  SendNavigatorParamList,
  "sendCrossShardRefund"
>
export type SentCrossShardRefundScreenNavigationProps = StackNavigationProp<
  SendNavigatorParamList,
  "sendCrossShardRefund"
>
