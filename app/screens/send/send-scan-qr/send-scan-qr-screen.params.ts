export type SendScanQRScreenParams = {
  sendCoinName: string
  formIsDirty: boolean
  cached: {
    coinName: string
    address: string
    amount: string
    shardID: string
  }
}
