import { observer } from "mobx-react-lite"
import React, { useEffect, useState } from "react"
import { StyleSheet } from "react-native"
import { BarCodeReadEvent } from "react-native-camera"
import QRCodeScanner from "react-native-qrcode-scanner"

import { StackActions, useNavigation, useRoute } from "@react-navigation/native"

import {
  Button,
  Header,
  HStack,
  Modal,
  Screen,
  ScreenContent,
  Text,
  Toast,
  VStack,
} from "../../../components/base"
import { useStores } from "../../../models"
import { SendScanQRScreenRouteProps } from "./send-scan-qr-screen.props"
import { translate } from "../../../i18n"

interface QRData {
  coinName: string
  address: string
  amount: string
  shardID: number
}

export const SendScanQRScreen = observer(() => {
  const navigation = useNavigation()

  const route = useRoute<SendScanQRScreenRouteProps>()
  const {
    sendCoinName,
    formIsDirty,
    cached = { coinName: undefined, address: undefined, amount: undefined, shardID: undefined },
  } = route.params ?? {
    sendCoinName: null,
    formIsDirty: null,
  }

  const [qrData, setQRData] = useState<QRData | null>(null)
  const [callbackMode, setCallbackMode] = useState<"address" | "full">(null)
  const [infoModalIsVisible, setInfoModalIsVisible] = useState(false)

  const { coinStore, balanceStore } = useStores()

  const onRead = (event: BarCodeReadEvent) => {
    let parsedData = {
      coinName: undefined,
      address: undefined,
      amount: undefined,
      shardID: undefined,
    }

    if (event.data.startsWith("peacewallet://send/")) {
      const [coinName, address, amount, shardID] = event.data
        .replace("peacewallet://send/", "")
        .split("/")

      parsedData = {
        coinName,
        address,
        amount,
        shardID,
      }
    } else {
      try {
        const { coinName, address, amount, shardID } = JSON.parse(event.data)

        parsedData = {
          coinName,
          address,
          amount,
          shardID,
        }
      } catch (error) {
        Toast.show({
          type: "error",
          text1: translate("notifications.error"),
          text2: translate("notifications.invalidQRCode"),
        })
      }
    }

    const { coinName, address, amount, shardID } = parsedData

    const shardIDs = [
      ...balanceStore
        .availableBalancesByCoin(coinStore.coins.find((coin) => coin.name === "jax"))
        .map((balance) => balance.shardID),
    ]

    const jaxShardIDIsValid = coinName === "jax" ? shardIDs.includes(Number(shardID)) : true

    const coinIsValid = coinName ? coinStore.coins.find((c) => c.name === coinName) : false
    const amountIsValid = amount ? !isNaN(amount) && amount > 0 : true
    const shardIDIsValid = shardID
      ? !isNaN(Number(shardID)) && Number(shardID) >= 0 && jaxShardIDIsValid
      : true

    if (!address || !coinIsValid || !amountIsValid || !shardIDIsValid) {
      Toast.show({
        type: "error",
        text1: translate("notifications.error"),
        text2: translate("notifications.invalidQRCode"),
      })
      return
    }

    setQRData(parsedData)

    if (sendCoinName && sendCoinName !== coinName) {
      setInfoModalIsVisible(true)
    } else {
      setCallbackMode("full")
    }
  }

  const renderInfoModal = () => (
    <Modal
      isVisible={infoModalIsVisible}
      onClosed={() => setInfoModalIsVisible(false)}
      FooterComponent={
        <VStack marginBottom={12} space="sm">
          <Button
            flex={1}
            textTx="buttons.useOnlyAddress"
            variant="subtle"
            onPress={() => {
              setCallbackMode("address")
              setInfoModalIsVisible(false)
            }}
          />
          <HStack space="xs">
            <Button
              flex={1}
              textTx="buttons.cancel"
              variant="ghost"
              onPress={() => setInfoModalIsVisible(false)}
            />
            <Button
              flex={1}
              textTx="buttons.continue"
              onPress={() => {
                setCallbackMode("full")
                setInfoModalIsVisible(false)
              }}
            />
          </HStack>
        </VStack>
      }
    >
      <Text
        textTx="texts.scanInvoiceWithDifferentCoin"
        textTxOptions={{
          qrCoinName: qrData ? qrData.coinName.toUpperCase() : "",
          selectedCoinName: sendCoinName ? sendCoinName.toUpperCase() : "",
        }}
        align="center"
      />
    </Modal>
  )

  const showInfoToast = () => {
    if (
      formIsDirty ||
      (callbackMode && sendCoinName !== null && sendCoinName !== coinStore.selectedCoin.name)
    ) {
      Toast.show({
        type: "info",
        text2: translate("notifications.sendFormDataChanged"),
      })
    }
  }

  useEffect(() => {
    if (qrData) {
      const { coinName, address, amount, shardID } = qrData

      if (callbackMode === "full") {
        const coin = coinStore.coins.find((coin) => coin.name === coinName)
        let _shardID = shardID

        if (callbackMode && sendCoinName !== null && sendCoinName !== coinName) {
          if (shardID === null || shardID === undefined) {
            _shardID = coin.shardID
          }
        }

        navigation.dispatch(
          StackActions.replace("sendStack", {
            screen: "sendCreate",
            params: {
              coinName,
              address,
              amount: amount ?? cached.amount,
              shardID: _shardID ?? cached.shardID,
            },
          }),
        )

        showInfoToast()
      }

      if (callbackMode === "address") {
        navigation.dispatch(
          StackActions.replace("sendStack", {
            screen: "sendCreate",
            params: {
              coinName: cached.coinName,
              address,
              amount: cached.amount,
              shardID: cached.shardID,
            },
          }),
        )

        showInfoToast()
      }
    }

    return () => {
      setCallbackMode(null)
    }
  }, [callbackMode, qrData])

  return (
    <Screen header={<Header titleTx="titles.scanQR" />} unsafeBottom>
      <ScreenContent paddingLess>
        {renderInfoModal()}

        <QRCodeScanner
          reactivate={true}
          reactivateTimeout={5000}
          cameraStyle={styles.camera}
          onRead={onRead}
        />
      </ScreenContent>
    </Screen>
  )
})

const styles = StyleSheet.create({
  camera: {
    height: "100%",
  },
})
