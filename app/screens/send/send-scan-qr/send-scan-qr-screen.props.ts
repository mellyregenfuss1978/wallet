import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { SendNavigatorParamList } from "../../../navigators/stacks"

export type SendScanQRScreenRouteProps = RouteProp<SendNavigatorParamList, "sendScanQR">
export type SendScanQRScreenNavigationProps = StackNavigationProp<
  SendNavigatorParamList,
  "sendScanQR"
>
