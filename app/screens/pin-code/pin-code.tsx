/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useRef, useState } from "react"
import PINCode, { hasUserSetPinCode } from "@haskkor/react-native-pincode"
import { Button, Icon, VStack } from "../../components/base"
import { useColorMode, useTheme } from "native-base"
import { Animated, Dimensions } from "react-native"
import { useStores } from "../../models"
import { observer } from "mobx-react-lite"
import { t } from "i18n-js"
import { SafeAreaView } from "react-native-safe-area-context"
import TouchID from "react-native-touch-id"

export const PinCodeScreen = observer(() => {
  const { colors } = useTheme()
  const { colorMode } = useColorMode()
  const rootStore = useStores()
  const { walletStore } = rootStore

  const [userHasPinCode, setUserHasPinCode] = useState(null)
  const [handledUserPinCode, setHandledUserPinCode] = useState(false)
  const [touchIDIsSupported, setTouchIDIsSupported] = useState(false)

  const fadeAnim = useRef(new Animated.Value(1)).current

  const onFinish = () => {
    fadeOut()
    setHandledUserPinCode(false)
    setTimeout(() => {
      rootStore.hidePinCodeScreen()
      fadeIn()
    }, 1000)
  }

  const handlePinCode = async (): Promise<boolean> => {
    const value = await hasUserSetPinCode()
    setUserHasPinCode(value)

    await setTimeout(() => {
      setHandledUserPinCode(true)
    }, 150)

    return value
  }

  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 150,
      useNativeDriver: false,
    }).start()
  }

  const fadeOut = () => {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 250,
      useNativeDriver: false,
    }).start()
  }

  useEffect(() => {
    if (rootStore.pinCodeScreenEnabled && rootStore.pinCodeScreenIsVisible) {
      handlePinCode()
    }
  }, [rootStore.pinCodeScreenEnabled, rootStore.pinCodeScreenIsVisible])

  useEffect(() => {
    ;(async () => {
      const hasPinCode = await handlePinCode()

      if (
        hasPinCode &&
        rootStore.pinCodeScreenEnabled &&
        (walletStore.mnemonic || walletStore.privateKey)
      ) {
        rootStore.showPinCodeScreen()
      }

      TouchID.isSupported()
        .then(() => setTouchIDIsSupported(true))
        .catch(() => setTouchIDIsSupported(false))
    })()
  }, [])

  return rootStore.pinCodeScreenEnabled &&
    rootStore.pinCodeScreenIsVisible &&
    handledUserPinCode ? (
    <Animated.View
      style={{
        opacity: fadeAnim,
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
      }}
    >
      <VStack
        position="absolute"
        top={0}
        bottom={0}
        left={0}
        right={0}
        alignItems="center"
        justifyContent="center"
        _light={{ bg: "offWhite" }}
        _dark={{ bg: "dark.500" }}
      >
        <SafeAreaView>
          <PINCode
            status={userHasPinCode ? "enter" : "choose"}
            disableLockScreen={true}
            finishProcess={onFinish}
            titleChoose={t("titles.createPinCode")}
            titleEnter={t("titles.enterPinCode")}
            titleConfirm={t("titles.confirmPinCode")}
            titleConfirmFailed={t("titles.confirmFailedPinCode")}
            titleAttemptFailed={t("titles.incorrectPinCode")}
            subtitleError={" "}
            subtitleChoose={" "}
            subtitleConfirm={" "}
            stylePinCodeCircle={{
              width: 24,
              height: 24,
              borderRadius: 8,
              borderWidth: 1,
              marginRight: 4,
              marginLeft: 4,
              borderColor: colorMode === "light" ? colors.primaryAlpha[50] : colors.whiteAlpha[50],
            }}
            colorPassword={colorMode === "light" ? colors.primary[600] : colors.primary[500]}
            colorPasswordEmpty={
              colorMode === "light" ? colors.primaryAlpha[50] : colors.whiteAlpha[50]
            }
            colorPasswordError={colorMode === "light" ? colors.error[500] : colors.error[500]}
            stylePinCodeColorTitleError={
              colorMode === "light" ? colors.error[500] : colors.error[500]
            }
            stylePinCodeTextTitle={{
              fontSize: 28,
              fontFamily: "Space Grotesk",
              fontWeight: "500",
            }}
            stylePinCodeColorTitle={colorMode === "light" ? colors.primary[600] : colors.white}
            styleMainContainer={{
              width: Dimensions.get("screen").width,
              paddingHorizontal: 8,
              justifyContent: "space-between",
            }}
            stylePinCodeButtonCircle={{
              width: "100%",
              margin: 0,
              padding: 0,
              height: 64,
              borderRadius: 12,
              borderWidth: 1,
              borderColor:
                colorMode === "light" ? colors.primaryAlpha["100"] : colors.whiteAlpha["50"],
            }}
            stylePinCodeTextButtonCircle={{
              fontSize: 16,
              fontFamily: "Space Grotesk",
              fontWeight: "500",
              color: colorMode === "light" ? colors.primary[600] : colors.white,
            }}
            stylePinCodeEmptyColumn={{
              width: Dimensions.get("screen").width / 3 - 10,
              marginLeft: 4,
              marginRight: 4,
            }}
            colorCircleButtons={colorMode === "light" ? colors.white : colors.darkGray["500"]}
            numbersButtonOverlayColor={
              colorMode === "light" ? colors.whiteAlpha["50"] : colors.darkGray["500"]
            }
            stylePinCodeRowButtons={{ height: 72 }}
            stylePinCodeColumnButtons={{
              width: Dimensions.get("screen").width / 3 - 10,
              marginLeft: 4,
              marginRight: 4,
            }}
            buttonNumberComponent={(number, setNumber) => (
              <Button
                width="100%"
                variant="subtle"
                _light={{ bgColor: colors.white }}
                text={number}
                onPress={() => setNumber(number)}
              />
            )}
            buttonDeleteComponent={(eraseCharacter) => (
              <Button
                width="100%"
                variant="subtle"
                _dark={{ bgColor: colors.darkGrayAlpha[500] }}
                onPress={() => eraseCharacter()}
              >
                <Icon.ArrowLeft />
              </Button>
            )}
            bottomLeftComponent={
              userHasPinCode && touchIDIsSupported
                ? (launchTouchID) => (
                    <Button
                      width="100%"
                      variant="subtle"
                      _dark={{ bgColor: colors.darkGrayAlpha[500] }}
                      onPress={launchTouchID}
                    >
                      <Icon.Fingerprint />
                    </Button>
                  )
                : null
            }
          />
        </SafeAreaView>
      </VStack>
    </Animated.View>
  ) : null
})
