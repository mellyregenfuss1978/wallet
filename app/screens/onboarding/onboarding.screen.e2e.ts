import { by, device, element, expect } from "detox"
import { toPercentage } from "i18n-js"

describe("Onboarding", () => {
  beforeAll(async () => {
    await device.launchApp()
  })

  beforeEach(async () => {
    await device.reloadReactNative()
  })

  it("should have onboarding screen", async () => {
    await expect(element(by.id("onboarding-screen"))).toBeVisible()
    
  })

  it("check carousel images", async () => {
    await expect(element(by.id("onboarding-screen"))).toBeVisible()
    await expect(element(by.id("world-image"))).toBeVisible()
    await element(by.id("onboarding-screen")).swipe("left", 'fast', 0.5)
    await expect(element(by.id("withdrawal-image"))).toBeVisible()
    await element(by.id("withdrawal-image")).swipe("left", "fast", 0.5)
    await expect(element(by.id("secure-image"))).toBeVisible()

  })

  it("should go to restore wallet screen after tap and go back", async () => {
    await element(by.id("restore-wallet-button")).tap()
    await expect(element(by.id("wallet-restore-screen"))).toBeVisible()
    await element(by.id("navigation-back-button")).tap()
    await expect(element(by.id("onboarding-screen"))).toBeVisible()
  })

  it("should go to create wallet screen after tap and go back", async () => {
    await element(by.id("create-wallet-button")).tap()
    await expect(element(by.id("wallet-create-screen"))).toBeVisible()
    await element(by.id("navigation-back-button")).tap()
    await expect(element(by.id("onboarding-screen"))).toBeVisible()
  })
})
