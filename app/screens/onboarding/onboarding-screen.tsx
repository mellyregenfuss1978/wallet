import { observer } from "mobx-react-lite"
import { useColorModeValue } from "native-base"
import React, { useRef, useState } from "react"
import { Dimensions, StyleSheet } from "react-native"
import Carousel from "react-native-reanimated-carousel"

import { useNavigation } from "@react-navigation/native"

import FastWithdrawal from "../../../assets/images/onboarding/fast-withdrawal.svg"
import SecureImage from "../../../assets/images/onboarding/secure.svg"
import WorldImage from "../../../assets/images/onboarding/world.svg"
import {
  Box,
  Button,
  Flex,
  HStack,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  VStack,
} from "../../components/base"
import { TxKeyPath } from "../../i18n"
import { theme } from "../../theme"

type OnboardingPage = {
  titleTx: TxKeyPath
  image: React.ReactNode
}

export const OnboardingScreen = observer(function WelcomeScreen() {
  const navigation = useNavigation()

  const carouselRef = useRef(null)
  const [selectedOnboardingPageIndex, setSelectedOnboardingPageIndex] = useState(0)

  const navigateToCreateWalletScreen = () => {
    navigation.navigate("walletStack", {
      screen: "walletCreate",
    })
  }

  const navigateToRestoreWalletByMnemonicScreen = () =>
    navigation.navigate("walletStack", {
      screen: "walletRestoreByMnemonic",
    })

  const onboardingPages: Array<OnboardingPage> = [
    {
      image: <WorldImage testID="world-image" />,
      titleTx: "texts.onboardingPage1",
    },
    {
      image: <FastWithdrawal testID="withdrawal-image" />,
      titleTx: "texts.onboardingPage2", 
    },
    {
      image: <SecureImage testID="secure-image" />,
      titleTx: "texts.onboardingPage3",
    },
  ]

  const renderCarouselItem = (item: OnboardingPage) => (
    <VStack flex={1} alignItems="center" space="2xl">
      <Spacer />
      {item.image}
      <HStack px="2">
        <Text align="center" fontSize={28} lineHeight="36px" textTx={item.titleTx} />
      </HStack>
      <Spacer />
    </VStack>
  )

  const renderCarousel = () => (
    <Carousel
      loop={false}
      ref={carouselRef}
      data={[...new Array(onboardingPages.length).keys()]}
      renderItem={({ index }) => renderCarouselItem(onboardingPages[index])}
      width={Dimensions.get("window").width}
      onSnapToItem={setSelectedOnboardingPageIndex}
    />
  )

  const renderCarouselPagination = () => (
    <>
      <HStack alignItems="center" justifyContent="center" space="sm">
        {onboardingPages.map((item, index) => (
          <Box
            key={`dot-${index}`}
            w={"16px"}
            h={"5px"}
            rounded="xs"
            bgColor={
              selectedOnboardingPageIndex === index
                ? useColorModeValue("primary.500", "primary.500")
                : useColorModeValue("primaryAlpha.200", "whiteAlpha.200")
            }
          />
        ))}
      </HStack>
    </>
  )

  return (
    <Screen testID="onboarding-screen">
      <ScreenContent>
        <VStack flex={1} space="md">
          <Flex flex={3} style={styles.carousel}>
            {renderCarousel()}
          </Flex>

          <Box py="8">{renderCarouselPagination()}</Box>

          <HStack space="xs">
            <Button
              flex={1}
              testID="restore-wallet-button"
              textTx="buttons.restoreWallet"
              variant="outline"
              borderRadius="md"
              _light={{ backgroundColor: "white" }}
              onPress={navigateToRestoreWalletByMnemonicScreen}
            />
            <Button
              flex={1}
              testID="create-wallet-button"
              textTx="buttons.createWallet"
              onPress={navigateToCreateWalletScreen}
            />
          </HStack>
        </VStack>
      </ScreenContent>
    </Screen>
  )
})

const styles = StyleSheet.create({
  carousel: {
    marginHorizontal: -theme.space[2],
  },
})
