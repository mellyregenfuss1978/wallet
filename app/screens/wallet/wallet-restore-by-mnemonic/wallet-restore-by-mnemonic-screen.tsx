import { observer } from "mobx-react-lite"
import React from "react"
import { Controller, useForm } from "react-hook-form"

import Clipboard from "@react-native-community/clipboard"
import { StackActions, useNavigation } from "@react-navigation/native"

import {
  Button,
  Card,
  Header,
  HStack,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  TextArea,
  VStack,
} from "../../../components/base"
import { TxKeyPath } from "../../../i18n"
import { useStores } from "../../../models"

export const WalletRestoreByMnemonicScreen = observer(() => {
  const navigation = useNavigation()
  const rootStore = useStores()
  const { walletStore } = rootStore

  const {
    control,
    setValue,
    trigger,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    mode: "onSubmit",
  })

  const navigateToRestoreByPrivateKeyScreen = () =>
    navigation.navigate("walletStack", {
      screen: "walletRestoreByPrivateKey",
    })

  const onSubmit = async (data) => {
    const { mnemonic } = data

    await walletStore.saveMnemonic(mnemonic)

    rootStore.showPinCodeScreen()

    navigation.dispatch(
      StackActions.replace("tabsStack", {
        screen: "balance",
      }),
    )
  }

  const onPasteFromClipboard = async () => {
    const text = (await Clipboard.getString()).replace(/\n/g, " ").trim()
    setValue("mnemonic", text)
    trigger("mnemonic")
  }

  const renderMnemonicCaption = () => {
    let textTx: TxKeyPath = null

    switch (errors.mnemonic?.type) {
      case "required":
        textTx = "errors.mnemonicIsRequired"
        break
      case "valid":
        textTx = "errors.mnemonicIsInvalid"
        break
      default:
        textTx = null
        break
    }

    return (
      textTx && (
        <HStack px="4" alignItems="center" space={2}>
          <Icon.Info size={3.5} color="red.500" />
          <Text textTx={textTx} fontSize="sm" color="red.500" />
        </HStack>
      )
    )
  }

  return (
    <Screen testID="wallet-restore-screen" header={<Header />}>
      <ScreenContent>
        <VStack flex={1} space="sm">
          <Text testID="Text" textTx="titles.walletRestore" fontSize={28} align="center" />

          <VStack testID="VStack" space="xs">
            <Card testID="Card" error={errors.mnemonic}>
              <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                  <TextArea
                    value={value}
                    variant="unstyled"
                    totalLines={3}
                    p="0"
                    size="lg"
                    error={errors.mnemonic}
                    placeholderTx="placeholders.walletRestoreByMnemonic"
                    onBlur={onBlur}
                    onChangeText={(text) => {
                      reset({ mnemonic: text })
                      onChange(text)
                    }}
                    testID="TextArea"
                  />
                )}
                name="mnemonic"
                rules={{
                  required: true,
                  validate: {
                    valid: (value) => {
                      return walletStore.isValidMnemonic(value)
                    },
                  },
                }}
              />
            </Card>

            {renderMnemonicCaption()}

            <HStack justifyContent="center">
              <Button
                testID="pasteButton"
                textTx="buttons.paste"
                size="md"
                variant="subtle"
                startIcon={<Icon.Paste />}
                onPress={onPasteFromClipboard}
              />
            </HStack>
          </VStack>

          <Spacer />

          <VStack space="sm">
            <Button
              variant="ghost"
              textTx="buttons.restoreWalletByPrivateKey"
              onPress={navigateToRestoreByPrivateKeyScreen}
            />
            <Button
              textTx="buttons.restoreWallet"
              isLoading={walletStore.isLoading}
              onPress={handleSubmit(onSubmit)}
            />
          </VStack>
        </VStack>
      </ScreenContent>
    </Screen>
  )
})
