import React from "react"

import Clipboard from "@react-native-community/clipboard"

import {
  Button,
  Header,
  HStack,
  Icon,
  Screen,
  ScreenContent,
  Spacer,
  Text,
  Toast,
  VStack
} from "../../../components/base"
import { MnemonicPhrase } from "../../../components/features"
import { useTabBarVisible } from "../../../hooks"
import { useStores } from "../../../models"

export const WalletBackupScreen = () => {
  const { walletStore } = useStores()
  const { mnemonic } = walletStore

  const onCopyPhrase = () => {
    Clipboard.setString(mnemonic)
    Toast.show({
      type: "info",
      text1: "Copied",
    })
  }

  useTabBarVisible(false)

  return (
    <Screen header={<Header />}>
      <ScreenContent>
        <VStack flex={1} space="lg">
          <Text textTx="titles.walletBackup" fontSize={28} align="center" />

          <HStack justifyContent="center" px="2">
            <Text textTx="texts.walletBackup" align="center" secondary />
          </HStack>

          <MnemonicPhrase mnemonic={mnemonic} />

          <Spacer />

          <Button
            textTx="buttons.copyPhrase"
            variant="outline"
            endIcon={<Icon.Copy />}
            onPress={onCopyPhrase}
          />
        </VStack>
      </ScreenContent>
    </Screen>
  )
}
