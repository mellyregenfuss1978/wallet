import { observer } from "mobx-react-lite"
import moment from "moment"
import React, { useCallback, useEffect, useRef, useState } from "react"
import { Platform, StyleSheet } from "react-native"
import numeral from "numeral"
import { StackActions, useNavigation } from "@react-navigation/native"

import {
  Box,
  Button,
  Card,
  Header,
  HStack,
  Icon,
  IconButton,
  Modal,
  Pressable,
  RefreshControl,
  Screen,
  ScreenFlatListContent,
  Spinner,
  Text,
  Toast,
  VStack,
} from "../../components/base"
import { CoinsCarousel, RefundButton, TransactionDetails } from "../../components/features"
import { useTabBarVisible } from "../../hooks"
import { translate } from "../../i18n"
import { Coin, useStores } from "../../models"
import { HistoryItem } from "../../models/history-item/history-item"
import { theme } from "../../theme"
import { delay } from "../../utils/delay"
import { CoinsFormatter } from "../../utils/formatters"
import { Popover } from "native-base"

export const BalanceScreen = observer(() => {
  const { walletStore, balanceStore, historyStore, coinStore } = useStores()
  const { address } = walletStore
  const { coins, selectedCoin, selectCoin } = coinStore
  const { historyLength, historyByDate } = historyStore
  const { balances } = balanceStore

  const navigation = useNavigation()

  const isFirstRun = useRef(true)

  const [silentRefreshing, setSilentRefreshing] = useState(false)
  const [refreshing, setRefreshing] = useState(false)
  const [shardIDs, setShardIDs] = useState([])
  const [historyIsLoadingMore, setHistoryIsLoadingMore] = useState(false)
  const [historyDetailModalIsVisible, setHistoryDetailModalIsVisible] = useState(false)
  const [selectedHistoryItem, setSelectedHistoryItem] = useState(null)
  const [historyUpdatedTime, setHistoryUpdatedTime] = useState(new Date())

  const navigateToSendScanQRScreen = () =>
    navigation.navigate("sendStack", {
      screen: "sendScanQR",
    })

  const navigateToLockedBalanceScreen = () =>
    navigation.navigate("lockedBalance", {
      shardIDs: [0],
    })

  const getHistorySections = (shardIDs: number[]) => {
    const sections = historyByDate(shardIDs)
    historyStore.updateHistoryLength(sections.length)
    return sections
  }

  const getData = useCallback(
    async (silentRefresh = false) => {
      if (!address) return

      historyStore.cancelToken.cancel()

      if (!balances.length) {
        await getBalances().then(() => getHistory(true, silentRefresh))
      } else {
        getBalances()
        await getHistory(true, silentRefresh)
      }
    },
    [selectedCoin, address],
  )

  const getBalances = async () => {
    if (!balances.length) setRefreshing(true)

    return balanceStore.getBalances(address).then(() => {
      setShardIDs([
        ...balanceStore.availableBalancesByCoin(selectedCoin).map((balance) => balance.shardID),
      ])
      setRefreshing(false)

      if (
        balanceStore.isError &&
        historyStore.error.kind !== "canceled" &&
        historyStore.error.kind !== "rejected"
      ) {
        Toast.show({
          type: "error",
          text1: translate("notifications.error"),
          text2: `${balanceStore.error.message}`,
        })
      }
    })
  }

  const getHistory = async (refresh: boolean, silentRefresh = false) => {
    const _shardIDs = [
      ...balanceStore.availableBalancesByCoin(selectedCoin).map((balance) => balance.shardID),
    ]

    return historyStore.getHistory(address, _shardIDs, refresh, silentRefresh).then((response) => {
      setHistoryUpdatedTime(new Date())

      if (
        historyStore.isError &&
        historyStore.error.kind !== "canceled" &&
        historyStore.error.kind !== "rejected"
      ) {
        Toast.show({
          type: "error",
          text1: translate("notifications.error"),
          text2: `${historyStore.error.message}`,
        })
      } else if (
        refresh &&
        response.kind === "ok" &&
        !response?.data?.history?.records.length &&
        response?.data?.history?.nearestDates?.previous
      ) {
        return historyStore.getHistory(address, _shardIDs, false, false)
      }
    })
  }

  const onRefresh = async () => {
    setRefreshing(true)

    if (!silentRefreshing) {
      historyStore.clear(shardIDs)
      await getData()
    }

    setRefreshing(false)
  }

  const onLoadMore = async () => {
    if (historyIsLoadingMore || historyStore.loadedAllPreviousItems) return
    setHistoryIsLoadingMore(true)
    await getHistory(false)
    setHistoryIsLoadingMore(false)
  }

  const onRefund = async (hash: string) => {
    setHistoryDetailModalIsVisible(false)
    await delay(150)

    if (hash) {
      navigation.navigate("sendStack", {
        screen: "sentSuccessfully",
        params: {
          address: address,
          sourceShardID: selectedHistoryItem.sourceShardID,
          sourceAmount: selectedHistoryItem.sourceAmount,
          transactionFee: selectedHistoryItem.transactionFee,
          transactionHash: hash,
        },
      })
    } else {
      navigation.navigate("sendStack", {
        screen: "sendCrossShardRefund",
        params: {
          shardID: selectedHistoryItem.sourceShardID,
          amount: selectedHistoryItem.sourceAmount,
          transactionFee: selectedHistoryItem.transactionFee,
        },
      })
    }
  }

  const onSnapToItem = (coin: Coin) => {
    selectCoin(coin)
    setShardIDs([...balanceStore.availableBalancesByCoin(coin).map((balance) => balance.shardID)])
  }

  const onHistoryItemPress = (item: HistoryItem) => {
    if (item.crossShardTemp) {
      navigation.navigate("sendStack", {
        screen: "sendCrossShardProcessing",
        params: {
          sourceShardID: item.sourceShardID,
          destinationShardID: item.destinationShardID,
          transactionHash: item.transactionHash,
        },
      })
    } else {
      setSelectedHistoryItem(item)
      setHistoryDetailModalIsVisible(true)
    }
  }

  const renderHistoryDetailModal = () => (
    <Modal
      isVisible={historyDetailModalIsVisible}
      HeaderComponent={
        selectedHistoryItem && (
          <HStack justifyContent="space-between">
            <Text
              textTx={selectedHistoryItem.direction === "in" ? "labels.received" : "labels.sent"}
              textTransform="uppercase"
            />
            <Text
              text={moment(selectedHistoryItem.timestamp).format("DD MMM, LT")}
              _light={{ color: "primaryAlpha.500" }}
              _dark={{ color: "whiteAlpha.500" }}
            />
          </HStack>
        )
      }
      FooterComponent={
        <HStack space="xs">
          {((selectedHistoryItem?.isCrossShard && selectedHistoryItem.confirmations <= 0) ||
            selectedHistoryItem?.isLock) &&
            selectedHistoryItem.direction === "out" && (
              <RefundButton
                flex={1}
                shardID={selectedHistoryItem.sourceShardID}
                transactionHex={selectedHistoryItem.refundTransactionHex}
                onRefund={onRefund}
              />
            )}
          <Button
            flex={1}
            textTx="buttons.close"
            variant="outline"
            onPress={() => setHistoryDetailModalIsVisible(false)}
          />
        </HStack>
      }
      onClosed={() => setHistoryDetailModalIsVisible(false)}
    >
      {selectedHistoryItem && (
        <TransactionDetails
          coin={selectedCoin}
          sourceAmount={selectedHistoryItem.sourceAmount}
          destinationAmount={selectedHistoryItem.destinationAmount}
          transactionFee={selectedHistoryItem.transactionFee}
          exchangeAgentFee={selectedHistoryItem.exchangeAgentFee}
          address={selectedHistoryItem.address}
          sourceShardID={selectedHistoryItem.sourceShardID}
          destinationShardID={selectedHistoryItem.destinationShardID}
          hash={selectedHistoryItem.transactionHash}
          confirmations={selectedHistoryItem.confirmations}
          blockNumber={selectedHistoryItem.blockNumber}
          direction={selectedHistoryItem.direction}
          lockTransactionHash={selectedHistoryItem.lockTransactionHash}
        />
      )}
    </Modal>
  )

  const renderPageHeader = () => (
    <VStack pt={2} space="lg">
      <CoinsCarousel
        coins={coins}
        initialIndex={coins.indexOf(selectedCoin)}
        onLockedBalancePress={() => navigateToLockedBalanceScreen()}
        onSnapToItem={onSnapToItem}
      />

      <HStack px="6" pb="4" justifyContent="space-between">
        <Text textTx="labels.transactions" textTransform="uppercase" />
        <Text
          textTx="labels.updatedAt"
          textTxOptions={{ time: moment(historyUpdatedTime).format("LT") }}
          secondary
          fontWeight={400}
        />
      </HStack>
    </VStack>
  )

  const formatHistorySectionDate = (date: string) => {
    const momentDate = moment(date)
    const formattedDate = momentDate.format(
      momentDate.year() === moment().year() ? "D MMM" : "D MMM YYYY",
    )

    return formattedDate
  }

  const renderHistoryItemIcon = (direction: "in" | "out" | "loop" | string) => {
    let content = <></>

    switch (direction) {
      case "loop":
        content = (
          <>
            <Icon.ArrowTopRight color={`coins.${selectedCoin.name}.500`} />
            <Icon.Refresh
              size={3}
              color={`coins.${selectedCoin.name}.500`}
              style={styles.listItemLoopIcon}
            />
          </>
        )
        break
      case "out":
        content = <Icon.ArrowTopRight color={`coins.${selectedCoin.name}.500`} />
        break
      case "in":
        content = <Icon.ArrowBottomLeft color={`coins.${selectedCoin.name}.500`} />
        break
    }

    return (
      <Box
        p="3"
        rounded="lg"
        borderWidth={1}
        alignItems="center"
        justifyContent="center"
        _light={{ borderColor: "primaryAlpha.50" }}
        _dark={{ borderColor: "whiteAlpha.100" }}
      >
        {content}
      </Box>
    )
  }

  const renderHistoryItem = ({ item: section, index }) => (
    <Box my="1" key={`list-section-${index}`} style={styles.listSection}>
      <Card padding={4}>
        <VStack space={4}>
          <Text text={formatHistorySectionDate(section.title)} secondary />

          <VStack space={5}>
            {section.items.map((item: HistoryItem) => {
              const sourceAmount = CoinsFormatter.toBiggestUnits(
                selectedCoin.name,
                item.isCrossShard ? item.crossShardAmountByDirection : item.sourceAmount,
              )
              const formattedSourceAmount =
                sourceAmount > 10000 ? numeral(sourceAmount).format("0.00a") : sourceAmount

              return (
                <Pressable
                  key={`list-item-${item.transactionHash}`}
                  onPress={() => onHistoryItemPress(item)}
                >
                  <HStack alignItems="stretch" space={2}>
                    {renderHistoryItemIcon(item.direction)}
                    <VStack flex={1} justifyContent="space-around">
                      <HStack alignItems="center">
                        <HStack flex={1} space={1}>
                          {sourceAmount > 10000 ? (
                            <Popover
                              trigger={(triggerProps) => {
                                return (
                                  <Pressable {...triggerProps}>
                                    <Text text={`${formattedSourceAmount}`} />
                                  </Pressable>
                                )
                              }}
                            >
                              <Popover.Content zIndex={9999}>
                                <Popover.Arrow />
                                <Popover.Body>
                                  <Text text={`${sourceAmount}`} fontSize={14} />
                                </Popover.Body>
                              </Popover.Content>
                            </Popover>
                          ) : (
                            <Text text={`${sourceAmount}`} />
                          )}
                          <Text
                            text={selectedCoin.title}
                            color={`coins.${selectedCoin.name}.500`}
                            fontWeight={400}
                          />
                        </HStack>
                        <Text
                          textTx={`labels.status${
                            !item.confirmations
                              ? item.isCrossShard && item.crossShardTemp
                                ? "InProgress"
                                : "Mempool"
                              : item.confirmations > 10
                              ? "Confirmed"
                              : "Unconfirmed"
                          }`}
                          secondary
                          fontWeight={400}
                          _light={{
                            color:
                              !item.confirmations || item.confirmations <= 10
                                ? "orange.400"
                                : "primaryAlpha.500",
                          }}
                          _dark={{
                            color:
                              !item.confirmations || item.confirmations <= 10
                                ? "orange.400"
                                : "whiteAlpha.500",
                          }}
                        />
                      </HStack>
                      <HStack justifyContent="space-between">
                        <Text
                          text={`${
                            item.direction === "in"
                              ? translate("labels.received")
                              : translate("labels.sent")
                          } ${
                            selectedCoin.name === "jax"
                              ? `• ${translate("labels.shard")} ${item.sourceShardID}`
                              : ""
                          } ${
                            item.isCrossShard
                              ? `→ ${translate("labels.shard")} ${item.destinationShardID}`
                              : ""
                          }`}
                          secondary
                          fontWeight={400}
                        />
                        <Text
                          text={moment(item.timestamp).format("LT")}
                          secondary
                          fontWeight={400}
                        />
                      </HStack>
                    </VStack>
                  </HStack>
                </Pressable>
              )
            })}
          </VStack>
        </VStack>
      </Card>
    </Box>
  )

  useEffect(() => {
    setSelectedHistoryItem(null)
    setHistoryDetailModalIsVisible(false)
  }, [selectedCoin])

  useEffect(() => {
    if (!historyDetailModalIsVisible) {
      setSelectedHistoryItem(null)
    }
  }, [historyDetailModalIsVisible])

  useEffect(() => {
    if (isFirstRun.current) {
      isFirstRun.current = false
      setTimeout(() => getData(), 1500)
    } else {
      getData()
    }

    const silentRefreshInterval = setInterval(async () => {
      if (!historyStore.isLoading && !historyIsLoadingMore) {
        setSilentRefreshing(true)
        await getData(true)
        setSilentRefreshing(false)
      }
    }, 120000)

    return () => {
      clearInterval(silentRefreshInterval)
    }
  }, [getData])

  useEffect(() => {
    if ((!walletStore.mnemonic || !walletStore.privateKey || !walletStore.wif) && !address) {
      navigation.dispatch(
        StackActions.replace("onboardingStack", {
          screen: "onboarding",
        }),
      )
    }
  }, [walletStore.mnemonic, walletStore.privateKey, walletStore.wif, address])

  useTabBarVisible(true)

  return (
    <Screen
      unsafeBottom
      header={
        <Header
          titleTx="titles.balance"
          hasBackAction={false}
          rightAction={
            <IconButton
              variant="unstyled"
              icon={<Icon.ScanQR />}
              onPress={navigateToSendScanQRScreen}
            />
          }
        />
      }
    >
      {renderHistoryDetailModal()}

      <ScreenFlatListContent
        data={getHistorySections(shardIDs)}
        paddingLess
        contentContainerStyle={styles.list}
        contentInset={{ bottom: 96 }}
        ListHeaderComponent={renderPageHeader()}
        ListEmptyComponent={
          <HStack mx="2" alignContent="center" justifyContent="center" height="56px">
            {historyStore.isLoading ? (
              <Spinner />
            ) : (
              <Card flex={1}>
                <Text textTx="texts.emptyOperations" align="center" secondary />
              </Card>
            )}
          </HStack>
        }
        ListFooterComponent={
          <HStack mx="2" my="1" alignContent="center" justifyContent="center" height="56px">
            {historyIsLoadingMore ||
            (!refreshing &&
              !historyIsLoadingMore &&
              historyStore.isLoading &&
              !silentRefreshing &&
              historyLength > 0) ? (
              <Spinner />
            ) : historyStore.loadedAllPreviousItems &&
              !historyIsLoadingMore &&
              !historyStore.isLoading &&
              historyLength > 0 ? (
              <Card flex={1}>
                <Text textTx="texts.loadedAllTransactions" align="center" secondary />
              </Card>
            ) : (
              <></>
            )}
          </HStack>
        }
        renderItem={renderHistoryItem}
        keyExtractor={(_, index) => `history-section-${index.toString()}`}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            titleTx="texts.balanceRefreshing"
          />
        }
        onEndReachedThreshold={0.3}
        onEndReached={onLoadMore}
      />
    </Screen>
  )
})

const styles = StyleSheet.create({
  list: {
    paddingBottom: Platform.OS === "android" ? 64 : 0,
  },
  listItemLoopIcon: {
    bottom: 4,
    position: "absolute",
    right: 4,
  },
  listSection: {
    paddingHorizontal: theme.space[2],
  },
})
