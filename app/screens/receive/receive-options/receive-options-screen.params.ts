export type ReceiveOptionsScreenParams = {
  amount?: string
  shardID?: string
}
