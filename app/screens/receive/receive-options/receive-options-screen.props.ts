import { RouteProp } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"

import { ReceiveNavigatorParamList } from "../../../navigators/stacks"

export type ReceiveOptionsScreenRouteProps = RouteProp<ReceiveNavigatorParamList, "receiveOptions">
export type ReceiveOptionsScreenNavigationProps = StackNavigationProp<
  ReceiveNavigatorParamList,
  "receiveOptions"
>
