export type ReceiveQRScreenParams = {
  amount?: string
  shardID?: string
}
